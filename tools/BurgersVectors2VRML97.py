#!/usr/bin/env python

# Creates a VRML file for rendering dislocation Burgers vectors.

# Author: Alexander Stukowski

from sys import *
from optparse import OptionParser
from DislocationNetwork import *

# Parse command line options
parser = OptionParser(usage="Usage: %prog inputfile outputfile")
options, args = parser.parse_args()
if len(args) < 2:
    parser.error("Incorrect number of command line arguments. Please specify an input dislocations file and an output VRML file.")

# Read the input file.
instream = stdin
if args[0] != '-': instream = open(args[0], "r")
network = loadDislocationNetwork(instream)

# Open the output file.
outstream = stdout
if args[1] != '-': outstream = open(args[1], "w")

outstream.write("#VRML V2.0 utf8\n")
outstream.write("Group {\n")
outstream.write("  children [\n")
outstream.write("  Shape {\n")
outstream.write("appearance Appearance {\n")
outstream.write("material Material {\n")
outstream.write("ambientIntensity .075\n")
outstream.write("diffuseColor 1 1 1\n")
outstream.write("specularColor 1 1 1\n")
outstream.write("shininess .02\n")
outstream.write("}\n")
outstream.write("}\n")
outstream.write("geometry IndexedLineSet {\n")

outstream.write("coord Coordinate {\n")
outstream.write("point [\n")

outstream.write("]\n")
outstream.write("}\n")
outstream.write("}\n")
outstream.write("]\n")
outstream.write("}\n")
outstream.close()
