///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include <calib/CALib.h>
#include <calib/input/TextParserStream.h>
#include <calib/input/atoms/AtomicStructureParser.h>
#include <calib/input/patterns/PatternCatalogReader.h>
#include <calib/context/CAContext.h>
#include <calib/atomic_structure/AtomicStructure.h>
#include <calib/atomic_structure/neighbor_list/NeighborList.h>
#include <calib/pattern/catalog/PatternCatalog.h>
#include <calib/output/atoms/AtomicStructureWriter.h>

using namespace CALib;
using namespace std;

void printHelp(ostream& stream)
{
	stream << "Reference Configuration Creation Tool" << endl;
	stream << "Author: Alexander Stukowski (stukowski@mm.tu-darmstadt.de)" << endl;
	stream << "Version: " << CA_LIB_VERSION_STRING << endl << endl;
	stream << "Usage: RefConfTool [options] <catalogfile> <inputfile>" << endl << endl;
	stream << "Parameters:" << endl << endl;
	stream << "    catalogfile        : Pattern catalog file" << endl;
	stream << "    inputfile          : Input atoms file" << endl;
	stream << endl << "Output options:" << endl << endl;
	stream << "    -atoms FIELDS FILE    :  Output processed atoms to a LAMMPS dump file" << endl;
	stream << endl << "Control options:" << endl << endl;
	stream << "    -pbc X Y Z            :  Specify periodic boundary conditions (X,Y,Z = 0/1)" << endl;
	stream << "    -offset X Y Z         :  Add an offset to all atomic positions prior to analysis" << endl;
	stream << "    -cutoff RADIUS        :  Build conventional neighbor lists with given cutoff radius" << endl;
	stream << "    -tolerance TOL        :  Do not move atoms above this displacement threshold" << endl;
	stream << endl;
}

struct IdealGrainDefinition
{
	string latticePatternName;
	const SuperPattern* latticePattern;
	int seedAtomTag;
	CAFloat latticeConstant;
	Matrix3 latticeVectors;
	Matrix3 orientation;
};

int main(int argc, char* argv[])
{
	try {
		string atomsOutputFile;
		vector<AtomicStructureWriter::DataField> atomsOutputFields;
		string atomsOutputFieldsString;
		AtomsPreprocessingParameters preprocessing;
		CAFloat neighborCutoff = 0;
		vector<IdealGrainDefinition> idealGrains;
		int mirrorSymmetryAxis = -1;
		int mirrorSymmetryAtoms[2];
		CAFloat mirrorSymmetryDisplacement[2];
		CAFloat displacementTolerance = CAFLOAT_MAX;

		// The following sets the locale to the standard "C" locale, which is independent of the user's system settings.
		locale::global(locale::classic());

		// Parse command line parameters.
		int iarg = 1;
		while(iarg < argc) {
			if(strcmp(argv[iarg], "-atoms") == 0) {
				if(iarg >= argc-2) {
					cerr << "Missing parameter(s) after " << argv[iarg] << endl;
					return 1;
				}
				atomsOutputFieldsString = argv[iarg+1];
				atomsOutputFile = argv[iarg+2];
				iarg += 3;
			}
			else if(strcmp(argv[iarg], "-cutoff") == 0) {
				if(iarg >= argc-1) {
					cerr << "Missing parameter after " << argv[iarg] << endl;
					return 1;
				}
				neighborCutoff = atof(argv[iarg+1]);
				iarg += 2;
			}
			else if(strcmp(argv[iarg], "-tolerance") == 0) {
				if(iarg >= argc-1) {
					cerr << "Missing parameter after " << argv[iarg] << endl;
					return 1;
				}
				displacementTolerance = atof(argv[iarg+1]);
				iarg += 2;
			}
			else if(strcmp(argv[iarg], "-pbc") == 0) {
				if(iarg >= argc-3) {
					cerr << "Missing parameter after " << argv[iarg] << endl;
					return 1;
				}
				preprocessing.pbc[0] = atoi(argv[iarg+1]);
				preprocessing.pbc[1] = atoi(argv[iarg+2]);
				preprocessing.pbc[2] = atoi(argv[iarg+3]);
				preprocessing.overridePBC = true;
				iarg += 4;
			}
			else if(strcmp(argv[iarg], "-offset") == 0) {
				if(iarg >= argc-3) {
					cerr << "Missing parameter after " << argv[iarg] << endl;
					return 1;
				}
				preprocessing.atomsOffset.x() = atof(argv[iarg+1]);
				preprocessing.atomsOffset.y() = atof(argv[iarg+2]);
				preprocessing.atomsOffset.z() = atof(argv[iarg+3]);
				iarg += 4;
			}
			else if(strcmp(argv[iarg], "-idealgrain") == 0) {
				if(iarg >= argc-4) {
					cerr << "Missing parameter after " << argv[iarg] << endl;
					return 1;
				}
				IdealGrainDefinition grain;
				grain.seedAtomTag = atoi(argv[iarg+1]);
				grain.latticePatternName = argv[iarg+2];
				grain.latticeConstant = atof(argv[iarg+3]);
				double x1, y1, z1, x2, y2, z2, x3, y3, z3;
				if(sscanf(argv[iarg+4], "(%lg %lg %lg) (%lg %lg %lg) (%lg %lg %lg)", &x1, &y1, &z1, &x2, &y2, &z2, &x3, &y3, &z3) != 9) {
					cerr << "Invalid grain orientation: " << argv[iarg+4] << endl;
					return 1;
				}
				grain.latticeVectors.col(0) = Vector3(x1, y1, z1);
				grain.latticeVectors.col(1) = Vector3(x2, y2, z2);
				grain.latticeVectors.col(2) = Vector3(x3, y3, z3);
				if(fabs(grain.latticeVectors.determinant()) <= CAFLOAT_EPSILON) {
					cerr <<  "Degenerate basis vectors: " << argv[iarg+4] << endl;
					return 1;
				}
				if(fabs(grain.latticeVectors.col(0).dot(grain.latticeVectors.col(1))) > CAFLOAT_EPSILON ||
					fabs(grain.latticeVectors.col(0).dot(grain.latticeVectors.col(2))) > CAFLOAT_EPSILON ||
					fabs(grain.latticeVectors.col(1).dot(grain.latticeVectors.col(2))) > CAFLOAT_EPSILON) {
					cerr << "Non-orthogonal basis vectors: " << argv[iarg+4] << endl;
					return 1;
				}
				Matrix3 invOrientation;
				invOrientation.col(0) = grain.latticeVectors.col(0).normalized();
				invOrientation.col(1) = grain.latticeVectors.col(1).normalized();
				invOrientation.col(2) = grain.latticeVectors.col(2).normalized();
				grain.orientation = invOrientation.inverse() * grain.latticeConstant;
				idealGrains.push_back(grain);
				iarg += 5;
			}
			else if(strcmp(argv[iarg], "-symmetric") == 0) {
				if(iarg >= argc-6) {
					cerr << "Missing parameter after " << argv[iarg] << endl;
					return 1;
				}
				if(argv[iarg+1][0] == 'x' || argv[iarg+1][0] == 'X')
					mirrorSymmetryAxis = 0;
				else if(argv[iarg+1][0] == 'y' || argv[iarg+1][0] == 'Y')
					mirrorSymmetryAxis = 1;
				else if(argv[iarg+1][0] == 'z' || argv[iarg+1][0] == 'Z')
					mirrorSymmetryAxis = 2;
				else {
					cerr << "Invalid symmetry axis: " << argv[iarg+1] << endl;
					return 1;
				}
				mirrorSymmetryAtoms[0] = atoi(argv[iarg+2]);
				mirrorSymmetryAtoms[1] = atoi(argv[iarg+3]);
				mirrorSymmetryDisplacement[0] = atof(argv[iarg+4]);
				mirrorSymmetryDisplacement[1] = atof(argv[iarg+5]);
				iarg += 6;
			}
			else if(argv[iarg][0] == '-') {
				cerr << "Invalid command line option: " << argv[iarg] << endl;
				return 1;
			}
			else break;
		}
		if(iarg + 2 != argc) {
			printHelp(cerr);
			return 1;
		}

		// Create main object.
		CAContext context;
		context.initContext(argc, argv, false);
		CALIB_ASSERT(context.isMaster());

		// Parse per-atom data fields to be output.
		istringstream ss0(atomsOutputFieldsString);
		string fieldName;
		while(getline(ss0, fieldName, ',')) {
			if(fieldName == "id")
				atomsOutputFields.push_back(AtomicStructureWriter::ATOM_ID);
			else if(fieldName == "type")
				atomsOutputFields.push_back(AtomicStructureWriter::ATOM_TYPE);
			else if(fieldName == "x")
				atomsOutputFields.push_back(AtomicStructureWriter::POS_X);
			else if(fieldName == "y")
				atomsOutputFields.push_back(AtomicStructureWriter::POS_Y);
			else if(fieldName == "z")
				atomsOutputFields.push_back(AtomicStructureWriter::POS_Z);
			else if(fieldName == "pattern")
				atomsOutputFields.push_back(AtomicStructureWriter::SUPER_PATTERN);
			else if(fieldName == "cluster")
				atomsOutputFields.push_back(AtomicStructureWriter::CLUSTER_ID);
			else
				context.raiseErrorAll("Invalid per-atom field after -atoms option: %s", fieldName.c_str());
		}

		// Load pattern catalog.
		PatternCatalog catalog(context);
		ifstream catalog_stream;
		context.msgLogger() << "Loading pattern catalog '" << argv[iarg+0] << "'" << endl;
		catalog_stream.open(argv[iarg+0]);
		if(!catalog_stream.is_open())
			context.raiseErrorOne("Failed to open pattern catalog file for reading. Filename was '%s'.", argv[iarg+0]);
		PatternCatalogReader(context).read(catalog_stream, catalog);

		// Determine list of patterns to search for.
		set<SuperPattern const*> superPatternsToSearchForSet;
		set<CoordinationPattern const*> coordinationPatternsToSearchForSet;
		BOOST_FOREACH(IdealGrainDefinition& grain, idealGrains) {
			grain.latticePattern = catalog.superPatternByName(grain.latticePatternName);
			if(grain.latticePattern == NULL)
				context.raiseErrorAll("Pattern with name '%s' not found in pattern catalog.", grain.latticePatternName.c_str());
			superPatternsToSearchForSet.insert(grain.latticePattern);
			BOOST_FOREACH(const SuperPatternNode& node, grain.latticePattern->nodes) {
				CALIB_ASSERT(node.coordinationPattern != NULL);
				coordinationPatternsToSearchForSet.insert(node.coordinationPattern);
			}
		}
		vector<const SuperPattern*> superPatternsToSearchFor;
		BOOST_FOREACH(const SuperPattern& spi, catalog.superPatterns()) {
			if(superPatternsToSearchForSet.find(&spi) != superPatternsToSearchForSet.end())
				superPatternsToSearchFor.push_back(&spi);
		}
		vector<const CoordinationPattern*> coordinationPatternsToSearchFor;
		BOOST_FOREACH(const CoordinationPattern& cpi, catalog.coordinationPatterns()) {
			if(coordinationPatternsToSearchForSet.find(&cpi) != coordinationPatternsToSearchForSet.end())
				coordinationPatternsToSearchFor.push_back(&cpi);
		}

		// Determine maximum number of neighbors per atom required for pattern analysis.
		int maxNeighbors = 0;
		BOOST_FOREACH(CoordinationPattern const* pattern, coordinationPatternsToSearchFor) {
			maxNeighbors = max(maxNeighbors, pattern->numNeighbors());
		}

		if(neighborCutoff <= 0.0)
			context.raiseErrorAll("Invalid neighbor cutoff. Please use the -cutoff option to specify a neighbor cutoff distance.");

		// Create structure and initialize control parameters.
		AtomicStructure structure(context, AtomicStructure::IMPLICIT_IMAGES);

		// Parse input file.
		AtomicStructureParser(structure, preprocessing).readAtomsFile(argv[iarg+1], &cin);

		// Build local nearest neighbor lists.
		NeighborList neighborList(structure);
		neighborList.buildNeighborLists(neighborCutoff, true, true, CA_MAX_PATTERN_NEIGHBORS);

		// Do coordination pattern matching.
		CoordinationPatternAnalysis coordinationPatternAnalysis(structure, neighborList, catalog);
		coordinationPatternAnalysis.searchCoordinationPatterns(coordinationPatternsToSearchFor, neighborCutoff);

		// Do multi-atom pattern matching.
		SuperPatternAnalysis superPatternAnalysis(coordinationPatternAnalysis);
		superPatternAnalysis.searchSuperPatterns(superPatternsToSearchFor);

		vector< pair<Vector3, int> > alteredPositions(structure.numTotalAtoms(), make_pair(Vector3::Zero(), 0));
		for(vector<IdealGrainDefinition>::iterator grain = idealGrains.begin(); grain != idealGrains.end(); ++grain) {
			context.msgLogger() << "---------------------------------------------------------" << endl;
			context.msgLogger() << "Seed atom: " << grain->seedAtomTag << endl;
			context.msgLogger() << "Lattice vectors: " << endl << grain->latticeVectors;
			context.msgLogger() << "Ideal lattice orientation:" << endl << grain->orientation;

			// Make sure periodic cell size is commensurate with periodic crystal lattice.
			context.msgLogger() << "Old simulation cell:" << endl << structure.simulationCellMatrix().linear() << endl;
			Matrix3 rotatedPatternCell = grain->orientation * grain->latticePattern->periodicCell;
			Matrix3 p = rotatedPatternCell.inverse() * structure.simulationCellMatrix().linear();
			context.msgLogger() << "Repeats for pattern '" << grain->latticePattern->name << ":" << endl << p;

			AffineTransformation newCell = structure.simulationCellMatrix();
			for(int dim = 0; dim < 3; dim++) {
				if(!structure.pbc(dim)) continue;
				double nx = floor(p.col(dim).x() + 0.5);
				double ny = floor(p.col(dim).y() + 0.5);
				double nz = floor(p.col(dim).z() + 0.5);
				newCell.linear().col(dim) = rotatedPatternCell * Vector3(nx,ny,nz);
			}
			structure.setSimulationCell(newCell, structure.pbcFlags());
			context.msgLogger() << "New simulation cell:" << endl << structure.simulationCellMatrix().linear() << endl;

			// Find seed atom in atomic structure.
			int seedAtom = -1;
			for(int atomIndex = 0; atomIndex < structure.numTotalAtoms(); atomIndex++) {
				if(structure.atomTag(atomIndex) == grain->seedAtomTag) {
					CALIB_ASSERT(seedAtom == -1);
					seedAtom = atomIndex;
				}
			}
			if(seedAtom == -1)
				context.raiseErrorAll("Seed atom with given tag does not exist. Structure has %i atoms.", structure.numLocalAtoms());

			if(superPatternAnalysis.atomCluster(seedAtom) == NULL)
				context.raiseErrorAll("Seed atom is not a crystalline atom.");

			// Get orientation of cluster.
			const SuperPattern& pattern = superPatternAnalysis.atomPattern(seedAtom);
			const SuperPatternNode& seedNode = superPatternAnalysis.atomPatternNode(seedAtom);
			Cluster* cluster = superPatternAnalysis.atomCluster(seedAtom);
			Matrix3 clusterOrientation = cluster->orientation;
			context.msgLogger() << "Found cluster " << *cluster << "  symmetry permutations: " << seedNode.spaceGroupEntries.size() << endl;
			context.msgLogger() << "Cluster orientation:" << endl << clusterOrientation;

			// Find the best matching lattice orientation by applying all symmetry transformations.
			CAFloat bestDiff = CAFLOAT_MAX;
			Matrix3 idealClusterOrientation;
			BOOST_FOREACH(const SpaceGroupElement& sge, seedNode.spaceGroupEntries) {
				Matrix3 rotatedOrientation = grain->orientation * sge.tm;
				CAFloat diff = 0;
				for(int i = 0; i < 3; i++)
					for(int j = 0; j < 3; j++)
						diff += fabs(rotatedOrientation(i,j) - clusterOrientation(i,j));
				if(diff < bestDiff) {
					bestDiff = diff;
					idealClusterOrientation = rotatedOrientation;
				}
			}
			context.msgLogger() << "Ideal cluster orientation:" << endl << idealClusterOrientation;
			context.msgLogger() << "Difference: " << bestDiff << endl;
			context.msgLogger() << "Adjusting atomic positions." << endl;

			deque<int> toBeProcessed(1, seedAtom);
			set<int> visitedAtoms;
			visitedAtoms.insert(seedAtom);
			do {
				int currentAtom = toBeProcessed.front();
				toBeProcessed.pop_front();
				CALIB_ASSERT(superPatternAnalysis.atomCluster(currentAtom) == cluster);

				Vector3 currentPos = structure.atomPosition(currentAtom) + alteredPositions[currentAtom].first;
				if(alteredPositions[currentAtom].second > 1) continue;

				const SuperPatternNode& node = superPatternAnalysis.atomPatternNode(currentAtom);
				for(int n = 0; n < node.coordinationPattern->numNeighbors(); n++) {
					int neighborAtom = neighborList.neighbor(currentAtom, superPatternAnalysis.nodeNeighborToAtomNeighbor(currentAtom, n));
					Vector3 idealPos = currentPos + (idealClusterOrientation * node.neighbors[n].referenceVector);
					Vector3 delta = structure.wrapVector(idealPos - structure.atomPosition(neighborAtom));
					if(delta.norm() < displacementTolerance && visitedAtoms.find(neighborAtom) == visitedAtoms.end()) {

						alteredPositions[neighborAtom].first += delta;
						alteredPositions[neighborAtom].second += 1;

						visitedAtoms.insert(neighborAtom);
						if(superPatternAnalysis.atomCluster(neighborAtom) == cluster) {
							toBeProcessed.push_back(neighborAtom);
						}
					}
				}
			}
			while(!toBeProcessed.empty());
			context.msgLogger() << "Adjusted the positions of " << visitedAtoms.size() << " atoms." << endl;
		}
		context.msgLogger() << "---------------------------------------------------------" << endl;

		// Write adjusted positions back into structure.
		for(int atomIndex = 0; atomIndex < structure.numTotalAtoms(); atomIndex++) {
			if(alteredPositions[atomIndex].second) {
				Vector3 displacement = alteredPositions[atomIndex].first / alteredPositions[atomIndex].second;
				structure.setAtomPosition(atomIndex, structure.atomPosition(atomIndex) + displacement);
			}
		}

		// Make structure symmetric.
		if(mirrorSymmetryAxis >= 0) {
			context.msgLogger() << "Correcting symmetry" << endl;
			context.msgLogger() << "---------------------------------------------------------" << endl;
			int taxis1 = (mirrorSymmetryAxis+1) % 3;
			int taxis2 = (mirrorSymmetryAxis+2) % 3;

			if(structure.pbc(mirrorSymmetryAxis))
				context.raiseErrorAll("Template structure must be non-periodic in symmetry direction.");
			if(!structure.pbc(taxis1) || !structure.pbc(taxis2))
				context.raiseErrorAll("Template structure must be 2d periodic when using the -symmetric option.");

			int atom1 = structure.atomIndexFromTag(mirrorSymmetryAtoms[0]);
			int atom2 = structure.atomIndexFromTag(mirrorSymmetryAtoms[1]);
			if(atom1 < 0 || atom2 < 0)
				context.raiseErrorAll("Atom indices passed to the -symmetric option are out of range.");

			CAFloat planePos = 0.5 * (structure.atomPosition(atom1)[mirrorSymmetryAxis] + structure.atomPosition(atom2)[mirrorSymmetryAxis]);
			context.msgLogger() << "Symmetry plane position: " << planePos << endl;

			Vector3 displacement = structure.reducedToAbsoluteVector(Vector3::Unit(taxis1) * mirrorSymmetryDisplacement[0] + Vector3::Unit(taxis2) * mirrorSymmetryDisplacement[1]);
			context.msgLogger() << "Symmetry displacement: " << displacement << endl;

			int nadjusted = 0;
			for(int atomIndex = 0; atomIndex < structure.numTotalAtoms(); atomIndex++) {
				const Vector3& p1 = structure.atomPosition(atomIndex);
				if(p1[mirrorSymmetryAxis] < planePos) continue;
				Vector3 p2 = p1 + displacement;
				p2[mirrorSymmetryAxis] = 2.0 * planePos - p1[mirrorSymmetryAxis];
				p2 = structure.wrapPoint(p2);
				for(int atomIndex2 = 0; atomIndex2 < structure.numTotalAtoms(); atomIndex2++) {
					Vector3 delta = structure.wrapVector(structure.atomPosition(atomIndex2) - p2);
					if(delta.squaredNorm() < 0.3) {
						structure.setAtomPosition(atomIndex2, p2);
						nadjusted++;
						break;
					}
				}
			}
			context.msgLogger() << "Adjusted the positions of " << nadjusted << " atoms." << endl;
		}

		// Dump adjusted atomic positions to output file.
		if(atomsOutputFile.empty() == false) {
			context.msgLogger() << "Writing processed atoms to output file '" << atomsOutputFile << "' " << endl;
			ofstream atoms_stream(atomsOutputFile.c_str());
			if(!atoms_stream)
				context.raiseErrorAll("Failed to open atoms file for writing. Filename was '%s'.", atomsOutputFile.c_str());
			atoms_stream.precision(12);
			AtomicStructureWriter writer(context);
			writer.writeLAMMPSFile(atoms_stream, atomsOutputFields, structure, &superPatternAnalysis);
		}
	}
	catch(const std::bad_alloc& ex) {
		cerr << endl << "ERROR: Out of memory" << endl;
		return 1;
	}
	catch(const exception& ex) {
		cerr << endl << "ERROR: " << ex.what() << endl;
		return 1;
	}

	return 0;
}
