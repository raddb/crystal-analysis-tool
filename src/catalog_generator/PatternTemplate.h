///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __PATTERN_TEMPLATE_H
#define __PATTERN_TEMPLATE_H

#include <calib/CALib.h>
#include <calib/context/CAContext.h>
#include <calib/pattern/coordinationpattern/CoordinationPattern.h>
#include <calib/pattern/coordinationpattern/CoordinationPatternAnalysis.h>
#include <calib/pattern/superpattern/SuperPattern.h>
#include <calib/pattern/superpattern/SuperPatternAnalysis.h>
#include <calib/pattern/catalog/PatternCatalog.h>
#include <calib/atomic_structure/AtomicStructure.h>
#include <calib/atomic_structure/neighbor_list/NeighborList.h>
#include <calib/input/atoms/AtomicStructureParser.h>

using namespace CALib;
using namespace std;

class InputFileParserStream;

/**
 * Describes a template for a structural pattern.
 */
struct PatternTemplate : public ContextReference
{
	/// Constructor.
	PatternTemplate(CAContext& context, const PatternCatalog& catalog) : ContextReference(context),
			_structure(context, AtomicStructure::IMPLICIT_IMAGES),
			_unrelaxedStructure(context, AtomicStructure::IMPLICIT_IMAGES),
			_neighborList(_structure),
			_coordAnalysis(_structure, _neighborList, catalog),
			_patternAnalysis(_coordAnalysis) {
		periodicity = -1;
		innerCutoff = 0;
		outerCutoff = 0;
		maxNDADisplacement = 0;
		perfectCoordinationMatchThreshold = 0.05;
		superpatternMatchThreshold = 1e-4;
		coordinationPatternType = CoordinationPattern::COORDINATION_PATTERN_NONE;
		vicinityCriterion = SuperPattern::VICINITY_CRITERION_LATTICE;
		cutoffGapFactor = 0.2;
		extendPatternShells = 0;
		preprocessing.overridePBC = true;
		preprocessing.pbc.assign(true);
		color = Vector3(0.7,0.8,1);
	}

	/// User-specified name of this pattern.
	string templateName;

	/// Human-readable name for the super pattern.
	string fullName;

	/// The number of dimensions in which the pattern is periodic (0,1,2,3).
	int periodicity;

	/// The type of coordination pattern generated from the template structure.
	CoordinationPattern::Type coordinationPatternType;

	/// The vicinity criterion to use during super pattern matching.
	SuperPattern::VicinityCriterion vicinityCriterion;

	/// The input file containing the template structure from which the pattern is generated.
	string templateFilename;

	/// The input file with the unrelaxed atom coordinates from which the lattice vectors of the pattern are calculated.
	/// This can be empty.
	string unrelaxedTemplateFilename;

	/// The inner cutoff that is used to determine the neighbors, which are included in coordination patterns.
	double innerCutoff;

	/// The outer cutoff radius that is used to build the initial neighbor lists.
	double outerCutoff;

	/// Factor multiplied by the distance between the last inner shell radius and the first outer shell radius to obtain the cutoff gap.
	double cutoffGapFactor;

	/// The maximum displacement of neighbor atoms (for the NDA).
	/// This is specified as a fraction of the radius of the first atom shell.
	double maxNDADisplacement;

	/// The threshold for the least-square fit up to which a coordination pattern match
	/// is considered a 'good match'.
	/// This specifies the maximum deviation of a neighbor vector as a fraction of the radius of the first atom shell.
	double perfectCoordinationMatchThreshold;

	/// The threshold for the least-square fit up to which a super pattern match
	/// is considered a 'good match'.
	/// This specifies the maximum deviation of a neighbor vector as a fraction of the radius of the first atom shell.
	double superpatternMatchThreshold;

	/// This controls how many extra atoms are included in the (defect) super pattern even though they match exactly to an existing
	/// super pattern.
	int extendPatternShells;

	/// If this is a defect pattern, then this list specifies the
	/// lattice patterns which are searched for first.
	vector<SuperPattern const*> surroundingLatticePatterns;

	/// List of optional clipping planes to restrict the pattern generation to a certain
	/// region of the template cell.
	vector<Plane3> clippingPlanes;

	/// List of defect pattern core atoms specifically selected by the user.
	vector<int> userCoreAtoms;

	/// Preprocessing of atoms.
	AtomsPreprocessingParameters preprocessing;

	/// The color of the pattern used for visualization purposes.
	Vector3 color;

	/// List of Burgers vector families defined by the user for this lattice.
	std::vector<BurgersVectorFamily> burgersVectorFamilies;

public:

	/// Parse the definition of a template from the input file.
	bool parseDefinition(InputFileParserStream& stream, PatternCatalog& catalog);

	/// Loads the template structure for the pattern from the specified atoms file.
	AtomicStructure& loadAtomicStructure();

	/// Returns the loaded template structure.
	const AtomicStructure& structure() const { return _structure; }

	/// Returns the loaded template structure.
	AtomicStructure& structure() { return _structure; }

	/// Returns the unrelaxed version of the template structure.
	const AtomicStructure& unrelaxedStructure() const { return _unrelaxedStructure; }

	/// Returns the unrelaxed version of the template structure.
	AtomicStructure& unrelaxedStructure() { return _unrelaxedStructure; }

	/// Returns the neighbor list for the template structure.
	const NeighborList& neighborList() const { return _neighborList; }

	/// Returns the neighbor list for the template structure.
	NeighborList& neighborList() { return _neighborList; }

	/// Given a neighbor of an atom, determines what the connecting vector looks like in the unrelaxed template structure.
	Vector3 unrelaxedNeighborVector(int atomIndex, int neighborIndex) const;

	/// Tests whether the given coordinate is located in the clipped region of the template cell.
	bool isClipped(const Vector3& pos) const {
		BOOST_FOREACH(const Plane3& plane, clippingPlanes) {
			if(plane.signedDistance(pos) > 0)
				return true;
		}
		return false;
	}

	/// Tests whether the given atom is located in the clipped region of the template cell.
	bool isClipped(int atomIndex) const {
		return isClipped(structure().atomPosition(atomIndex));
	}

	/// Create a graph pattern for a lattice structure.
	void generateLatticeGraphPattern(PatternCatalog& catalog, const CoordinationPatternAnalysis& coordPatternAnalysis);

	/// Create a graph pattern for a crystal defect.
	void generateDefectGraphPattern(PatternCatalog& catalog, const CoordinationPatternAnalysis& coordPatternAnalysis);

	/// Create a graph pattern for a crystal defect.
	void generateDefectGraphPattern2(PatternCatalog& catalog, const CoordinationPatternAnalysis& coordPatternAnalysis);

	/// Generates a new graph pattern for the given set of atoms.
	auto_ptr<SuperPattern> createGraphPattern(const vector<int>& listOfAtoms, const vector<CoordinationPatternAnalysis::coordmatch const*>& coordMappings, int numCoreNodes,
			PatternCatalog& catalog, const SuperPatternAnalysis& superPatternAnalysis);

	/// Finds all symmetry elements of the pattern's space group.
	void findSpaceGroupElements(SuperPattern& pattern, PatternCatalog& catalog, const vector<int>& listOfAtoms, const CoordinationPatternAnalysis& coordAnalysis);

	/// Quantifies the deviation of an actual coordination structure of an atom from the ideal reference pattern.
	CAFloat calculateSuperPatternDeviation(int atomIndex, const SuperPatternAnalysis& superPatternAnalysis) const;

private:

	/// The atomic structure that serves as a template for the pattern.
	AtomicStructure _structure;

	/// The neighbor list for the template structure.
	NeighborList _neighborList;

	/// The unrelaxed atomic structure that serves as a template for the pattern.
	AtomicStructure _unrelaxedStructure;

	/// This class takes care of analyzing the coordination structure of individual atoms.
	CoordinationPatternAnalysis _coordAnalysis;

	/// Used to find occurrences of generated patterns.
	SuperPatternAnalysis _patternAnalysis;

	/// Checks if the first token matches the given keyword,
	/// and if yes, checks that the right number of parameter tokens is present.
	bool expectParameters(const vector<string>& tokens, const char* keyword, int numParameters, InputFileParserStream& stream);
};

#endif // __PATTERN_TEMPLATE_H
