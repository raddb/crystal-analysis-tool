///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_CLUSTER_STRUCT_H
#define __CA_CLUSTER_STRUCT_H

#include "../pattern/superpattern/SuperPattern.h"

namespace CALib {

struct Cluster;
struct ClusterTransition;

/**
 * An identifier that is unique across processors.
 *
 * The identifier consists of two parts: a local identifier (unique on the current processor)
 * and the processor where the cluster was created. Together they are globally unique.
 */
struct ClusterId
{
	// The locally unique identifier.
	int id;

	// The processor the cluster was created on.
	int processor;

	/// Default constructor. Doesn't initialize the fields.
	ClusterId() {}

	/// Initialization constructor.
	ClusterId(int _id, int _proc) : id(_id), processor(_proc) {}

	/// Comparison and ordering operators.
	bool operator==(const ClusterId& other) const { return id == other.id && processor == other.processor; }
	bool operator!=(const ClusterId& other) const { return id != other.id || processor != other.processor; }
	bool operator<(const ClusterId& other) const { return id < other.id || (id == other.id && processor < other.processor); }
	bool operator>(const ClusterId& other) const { return id > other.id || (id == other.id && processor > other.processor); }
};

/// Prints a ClusterId to a log stream.
inline std::ostream& operator<<(std::ostream& stream, const ClusterId& cid) {
	return stream << "(" << cid.id << "," << cid.processor << ")";
}

/**
 * A cluster transition T_12 is a transformation matrix that connects the
 * reference frames of two cluster 1 and 2.
 *
 * For every cluster transition T_12, there exists a reverse transition
 * T_21 = (T_12)^-1.
 *
 * If clusters 1 and 2 are adjacent in the input structure then we can determine
 * the transition matrix T_12 from the overlap atoms at the border of the common two
 * clusters. Such a cluster transition T_12 corresponds to a directed edge in the cluster graph
 * that connects the two cluster nodes.
 *
 * Given two cluster transitions T_12 and T_23, we can construct a third
 * cluster transition T_13 = T_23 * T_12, that connects clusters 1 and 2.
 *
 * Every cluster has a so-called self-transition (or identity transition), which corresponds
 * to the identity matrix, and which is the reverse of itself.
 */
struct ClusterTransition
{
	/// The first cluster.
	/// The transformation matrix transforms vectors from this cluster to the coordinate system of cluster 2.
	Cluster* cluster1;

	/// The second cluster.
	/// The transformation matrix transforms vectors from cluster 1 to the coordinate system of this cluster.
	Cluster* cluster2;

	/// The transformation matrix that transforms vectors from the reference frame of cluster 1 to the frame
	/// of cluster 2.
	Matrix3 tm;

	/// Pointer to the reverse transition from cluster 2 to cluster 1.
	/// The transformation matrix of the reverse transition is the inverse of this transition's matrix.
	ClusterTransition* reverse;

	/// The cluster transitions form the directed edges of the cluster cluster graph (with the clusters being the nodes).
	/// Each node's outgoing edges are stored in a linked list. This field points
	/// to the next element in the linked list of cluster 1.
	ClusterTransition* next;

	/// The distance of clusters 1 and 2 in the cluster graph.
	/// The cluster transition is of distance 1 if the two cluster are immediate
	/// neighbors (i.e. they have a common border formed by overlap atoms in the atomic input structure).
	/// From two transitions A->B and B->C we can derive a new transition A->C, which
	/// is the concatenation of the first two. The distance associated with the transition
	/// A->C is the sum of distances of A->B and B->C.
	/// The distance of a self-transition A->A is defined to be zero.
	int distance;

	/// Returns true if this is the self-transition that connects a cluster with itself.
	/// The transformation matrix of an identity transition is always the identity matrix.
	bool isSelfTransition() const {
		CALIB_ASSERT((reverse != this) || (cluster1 == cluster2));
		CALIB_ASSERT((reverse != this) || (tm - Matrix3::Identity()).isZero(CAFLOAT_EPSILON));
		return reverse == this;
	}

	/// Transforms a vector from coordinate space of cluster 1 to the coordinate space of cluster 2.
	Vector3 transform(const Vector3& v) const {
		if(isSelfTransition()) return v;
		else return (tm * v);
	}

	/// Back-transforms a vector from coordinate space of cluster 2 to the coordinate space of cluster 1.
	Vector3 reverseTransform(const Vector3& v) const {
		if(isSelfTransition()) return v;
		else return (reverse->tm * v);
	}
};

inline bool clusterTransitionDistanceCompare(ClusterTransition* t1, ClusterTransition* t2);

/**
 * A cluster is a connected set of atoms in the input structure that all match to
 * one super pattern, i.e. they form a contiguous arrangement with long-range order.
 *
 * A cluster constitutes a node in the the so-called cluster graph, which is generated
 * during the pattern matching procedure.
 *
 * Every cluster is associated with an internal frame of reference (which is implicitly defined by the
 * template structure used to create the atomic pattern). When a cluster is created for an atomic
 * arrangement during the pattern matching procedure, an average orientation matrix is
 * calculated that transforms vectors from the cluster's reference frame to the global
 * simulation frame (in a least-square sense).
 *
 * Two clusters that are immediately adjacent in the input structure may have a specific
 * crystallographic orientation relationship, which can be determined from the overlap atoms
 * at their common border. Thus, vectors given in the local coordinate frame of one cluster
 * can be transformed to the other cluster's coordinate space. The corresponding transformation
 * matrix is referred to as a 'cluster transition', which constitutes a directed edge in the cluster graph,
 * which connects the two cluster nodes.
 */
struct Cluster
{
	/// The local identifier of the cluster and the processor it was created on.
	ClusterId id;

	/// The globally unique identifier of this (root) cluster.
	int outputId;

	/// The pattern formed by the atoms belonging to this cluster.
	///
	/// This is usually NULL for super clusters unless their children all have
	/// the same pattern and their atoms form one truly contiguous arrangement.
	/// This is the case if an atomic structure has been split into several parts because
	/// it is located on a processor domain boundary (e.g. a grain that spans multiple
	/// processors).
	SuperPattern const* pattern;

	/// The number of atoms from the input structure that are part of the cluster.
	AtomInteger atomCount;

	/// Linked list of transitions from this cluster to other clusters. They form the edges
	/// of the cluster graph.
	///
	/// The elements in the linked list are always ordered in ascending distance order.
	/// Thus, the self-transition (having distance 0) will always be a the head of the linked list
	/// if it has already been created.
	ClusterTransition* transitions;

	/// If this cluster is a child of a parent cluster, then this specifies the transition to
	/// coordinate frame of the parent cluster.
	ClusterTransition* parentTransition;

	/// This is a work variable used only during a recursive path search in the
	/// cluster graph. It points to the preceding node in the path.
	ClusterTransition* predecessor;

	/// This is a work variable used only during a recursive shortest path search in the
	/// cluster graph. It keeps track of the distance of this cluster from the
	/// start node of the path search.
	int distanceFromStart;

	/// Transformation matrix that transforms vectors from the cluster's internal coordinate space
	/// to the global simulation frame. Note that this describes the (average) orientation of the
	/// atomic arrangement in the simulation coordinate system.
	Matrix3 orientation;

	/// The center of mass of the cluster. This is computed from the atoms
	/// that are part of the cluster.
	Vector3 centerOfMass;

	/// Constructor.
	Cluster(ClusterId _id, SuperPattern const* _pattern = NULL) : id(_id),
			outputId(-1), pattern(_pattern), atomCount(0), transitions(NULL),
			parentTransition(NULL) {}

	/// Returns true if this cluster has no parent cluster.
	inline bool isRoot() const {
		return parentTransition->isSelfTransition();
	}

	/// Returns the parent of this cluster.
	inline Cluster* parentCluster() const {
		return parentTransition->cluster2;
	}

	/// Inserts a transition into this cluster's list of transitions.
	void insertTransition(ClusterTransition* newTransition) {
		CALIB_ASSERT(newTransition->cluster1 == this);
		// Determine the point of insertion to keep the linked list of
		// transitions sorted.
		ClusterTransition* appendAfter = NULL;
		for(ClusterTransition* t = this->transitions; t != NULL && clusterTransitionDistanceCompare(t, newTransition); t = t->next)
			appendAfter = t;
		if(appendAfter != NULL) {
			newTransition->next = appendAfter->next;
			appendAfter->next = newTransition;
			CALIB_ASSERT(clusterTransitionDistanceCompare(appendAfter, newTransition));
		}
		else {
			newTransition->next = this->transitions;
			this->transitions = newTransition;
		}
	}

	/// Removes a transition from the cluster's list of transitions.
	void removeTransition(ClusterTransition* t) {
		if(this->transitions == t) {
			this->transitions = t->next;
			t->next = NULL;
			return;
		}
		for(ClusterTransition* iter = this->transitions; iter != NULL; iter = iter->next) {
			if(iter->next == t) {
				iter->next = t->next;
				t->next = NULL;
				return;
			}
		}
		CALIB_ASSERT(false);	// Transition was not in the list.
	}

	/// Returns the transition to the given cluster or NULL if there is no direct transition.
	ClusterTransition* findTransition(Cluster* clusterB) const {
		for(ClusterTransition* t = transitions; t != NULL; t = t->next) {
			if(t->cluster2 == clusterB)
				return t;
		}
		return NULL;
	}

	/// Returns true if the given transition is in this cluster's list of transitions.
	bool hasTransition(ClusterTransition* transition) const {
		for(ClusterTransition* t = transitions; t != NULL; t = t->next) {
			if(t == transition) return true;
		}
		return false;
	}
};

/// This comparison function orders two clusters with respect to their path distance from the head node.
/// It is used during a shortest path search in the cluster graph.
/// If the distance from the head node is equal for the two clusters, then the IDs of the clusters
/// are used as a unique tie-breaker.
inline bool clusterGraphDistanceCompare(Cluster* clusterA, Cluster* clusterB) {
	if(clusterA->distanceFromStart != clusterB->distanceFromStart)
		return clusterA->distanceFromStart < clusterB->distanceFromStart;
	else
		return clusterA->id < clusterB->id;
}

/// This comparison function order two cluster transitions with respect to their distance (i.e. path length in
/// the cluster graph). It is used to sort the list of cluster transitions belonging to a cluster.
inline bool clusterTransitionDistanceCompare(ClusterTransition* t1, ClusterTransition* t2) {
	return t1->distance < t2->distance;
}

/// Outputs information about a cluster to a log stream.
inline std::ostream& operator<<(std::ostream& stream, const Cluster& c)
{
	stream << "(" << c.id.id << "," << c.id.processor << ", " << (c.isRoot() ? "root" : "child") << ", #" << c.atomCount << ", ";
	stream << (c.pattern == NULL ? std::string("non") : c.pattern->name);
	stream << ")";
	return stream;
}

}; // End of namespace

#endif // __CA_CLUSTER_STRUCT_H
