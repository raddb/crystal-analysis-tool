///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "ElasticField.h"
#include "../atomic_structure/AtomicStructure.h"
#include "../context/CAContext.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Constructor,
******************************************************************************/
ElasticField::ElasticField(CAContext& context, AtomicStructure& structure, DelaunayTessellation& tessellation) :
	Field(context, structure, tessellation)
{
	CALIB_ASSERT(&structure == &tessellation.structure());
}

/******************************************************************************
* Calculates the elastic deformation gradient field.
******************************************************************************/
void ElasticField::calculateElasticField(const ElasticMapping& elasticMapping)
{
	// Allocate output array.
	_elasticDeformationGradients.resize(localCells().size());

	vector<Matrix3>::iterator F_elastic = _elasticDeformationGradients.begin();
	for(vector<DelaunayTessellation::CellHandle>::const_iterator cell = localCells().begin(); cell != localCells().end(); ++cell, ++F_elastic) {

		F_elastic->setZero();

		// Determine local elastic deformation gradient.
		elasticMapping.computeElasticMapping(*cell, *F_elastic);
	}
}

}; // End of namespace
