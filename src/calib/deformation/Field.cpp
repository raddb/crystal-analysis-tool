///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "Field.h"

using namespace std;

namespace CALib {

// List of vertices that bound the six edges of a tetrahedron.
static const int edgeVertices[6][2] = {{0,1},{0,2},{0,3},{1,2},{1,3},{2,3}};

/******************************************************************************
* Constructor,
******************************************************************************/
Field::Field(CAContext& context, AtomicStructure& structure, DelaunayTessellation& tessellation) :
	ContextReference(context), _structure(structure), _tessellation(tessellation)
{
}

/******************************************************************************
* Generates the list of cells owned by the local processor.
******************************************************************************/
void Field::buildLocalCellList()
{
	_localCells.clear();
	for(DelaunayTessellation::CellIterator cell = tessellation().begin_cells(); cell != tessellation().end_cells(); ++cell) {
		if(isValidAndLocalCell(cell))
			_localCells.push_back(cell);
	}
}


}; // End of namespace
