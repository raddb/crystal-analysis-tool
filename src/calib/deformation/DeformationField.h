///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_DEFORMATION_FIELD_H
#define __CA_DEFORMATION_FIELD_H

#include "../CALib.h"
#include "Field.h"
#include "DisplacementVectors.h"
#include "../tessellation/ElasticMapping.h"

namespace CALib {

/**
 * Calculates the total deformation gradient field based on the tessellation of the atomistic system.
 *
 * Optional the total deformation field can be decomposed in elastic and plastic components.
 */
class DeformationField : public Field
{
public:

	/// Constructor.
	DeformationField(CAContext& context, AtomicStructure& structure1, AtomicStructure& structure2,
			DelaunayTessellation& tessellation, const PatternCatalog& catalog);

	/// Calculates the total deformation tensor field.
	void calculateTotalDeformation();

	/// Calculates the incremental elastic and plastic deformation tensor fields.
	void calculateElasticPlasticDeformation(const ElasticMapping& initialMapping, const ElasticMapping& finalMapping);

	/// Returns the initial configuration of the system.
	const AtomicStructure& structure1() const { return structure(); }

	/// Returns the final configuration of the system.
	const AtomicStructure& structure2() const { return _structure2; }

	/// Returns an index map that maps atom indices from the reference to the deformed configuration.
	const std::vector<int>& initialToFinalMap() const { return _displacements.initialToFinalMap(); }

	/// Returns an index map that maps atom indices from the deformed to reference configuration.
	const std::vector<int>& finalToInitialMap() const { return _displacements.finalToInitialMap(); }

	/// Returns the total deformation gradient tensors calculated for each local tessellation element.
	const std::vector<Matrix3>& deformationGradients() const { return _deformationGradients; }

	/// Returns the plastic deformation gradient tensors calculated for each local tessellation element.
	const std::vector<Matrix3>& plasticDeformationGradients() const { return _plasticDeformationGradients; }

	/// Returns the slip deformation gradient tensors calculated for each local tessellation element.
	const std::vector<Matrix3>& slipDeformationGradients() const { return _slipDeformationGradients; }

private:

	/// Builds a mapping between clusters in the initial configuration and clusters in the final configuration.
	void mapClusters(const ElasticMapping& initialMapping, const ElasticMapping& finalMapping);

	/// Determines the rigid body rotation of the crystal at an atom.
	bool determineClusterRotation(const ElasticMapping& initialMapping, const ElasticMapping& finalMapping, int atom_initial, int atom_final, Matrix3& tm) const;

private:

	/// The final configuration of the system.
	const AtomicStructure& _structure2;

	/// Computes the atomic displacement vectors.
	DisplacementVectors _displacements;

	/// The total deformation gradient tensor calculated for each tessellation element.
	std::vector<Matrix3> _deformationGradients;

	/// The plastic part of the deformation gradient tensors.
	std::vector<Matrix3> _plasticDeformationGradients;

	/// The slip deformation gradient tensors.
	std::vector<Matrix3> _slipDeformationGradients;

	/// Maps atom indices from initial to final configuration.
	std::vector<int> _initialToFinal;

	/// Maps atom indices from final to initial configuration.
	std::vector<int> _finalToInitial;

	/// Stores the transitions between clusters of the initial and final configuration.
	ClusterGraph _bridgedClusterGraph;
};

}; // End of namespace

#endif // __CA_DEFORMATION_FIELD_H

