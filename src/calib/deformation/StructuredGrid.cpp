///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "StructuredGrid.h"
#include "../atomic_structure/AtomicStructure.h"
#include "../context/CAContext.h"
#include "../util/Timer.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Constructor,
******************************************************************************/
StructuredGrid::StructuredGrid(CAContext& _context, AtomicStructure& _structure, CAFloat cellSize) :
	ContextReference(_context),
	structure(_structure)
{
	CALIB_ASSERT(cellSize > 0.0);

	// Determine the grid size based on the cell size parameter.
	for(unsigned int dim = 0; dim < 3; dim++) {
		dimensions[dim] = max((int)ceil(Length(structure.simulationCell().column(dim)) / cellSize), 1);
		gridToAbsoluteTM.setColumn(dim, structure.simulationCell().column(dim) / dimensions[dim]);
		if(structure.pbc(dim) == false) dimensions[dim] += 1;
	}
	absoluteToGridTM = gridToAbsoluteTM.inverse();
	absoluteToGridOffset = structure.simulationCellOrigin() - gridToAbsoluteTM * Vector3(0.5);

	// Allocate data array.
	data.resize(dimensions.X * dimensions.Y * dimensions.Z);
}

/******************************************************************************
* Projects the cell values of the given unstructured grid onto this structured grid.
******************************************************************************/
void StructuredGrid::project(const StrainCalculator& strainCalculator)
{
	const Tessellation& inputGrid = strainCalculator.getTessellation();

#if ANALYSIS_OUTPUT_LEVEL >= 1
	context().msgLogger() << "Projecting strain field to structured grid. Number of grid points: " << data.size() << endl;
	Timer projectionTimer;
#endif

	// Reset data array.
	for(vector<DataPoint>::iterator d = data.begin(); d != data.end(); ++d) {
		d->deformationGradient = NULL_MATRIX;
		d->plasticDeformationGradient = NULL_MATRIX;
		d->volume = 0;
	}

	// Project each tetrahedron to the grid one by one.
	int tetIndex = 0;
	for(Tessellation::CellIterator cell = inputGrid.begin_cells(); cell != inputGrid.end_cells(); ++cell) {
		if(cell->testFlag(Tessellation::IS_LOCAL_CELL) == false) continue;

		// Determine bounding box of tetrahedron.
		Point3I bbox_min(numeric_limits<int>::max());
		Point3I bbox_max(numeric_limits<int>::min());
		for(int v = 0; v < 4; v++) {
			InputAtom* atom = cell->vertex(v)->point().atom;
			CALIB_ASSERT(atom != NULL);
			Point3I gridPoint = absoluteToGrid(atom->pos);
			for(int dim = 0; dim < 3; dim++) {
				if(gridPoint[dim] < bbox_min[dim]) bbox_min[dim] = gridPoint[dim];
				if(gridPoint[dim] > bbox_max[dim]) bbox_max[dim] = gridPoint[dim];
			}
		}
		for(int dim = 0; dim < 3; dim++) {
			CALIB_ASSERT(bbox_min[dim] <= bbox_max[dim]);
			if(structure.pbc(dim) == false) {
				if(bbox_min[dim] < 0) bbox_min[dim] = 0;
				if(bbox_max[dim] >= dimensions[dim]) bbox_max[dim] = dimensions[dim]-1;
			}
		}

		Point3I p, pwrapped;
		for(p.X = bbox_min.X; p.X <= bbox_max.X; p.X++) {
			pwrapped.X = p.X;
			while(pwrapped.X < 0) pwrapped.X += dimensions.X;
			while(pwrapped.X >= dimensions.X) pwrapped.X -= dimensions.X;
			for(p.Y = bbox_min.Y; p.Y <= bbox_max.Y; p.Y++) {
				pwrapped.Y = p.Y;
				while(pwrapped.Y < 0) pwrapped.Y += dimensions.Y;
				while(pwrapped.Y >= dimensions.Y) pwrapped.Y -= dimensions.Y;
				for(p.Z = bbox_min.Z; p.Z <= bbox_max.Z; p.Z++) {
					pwrapped.Z = p.Z;
					while(pwrapped.Z < 0) pwrapped.Z += dimensions.Z;
					while(pwrapped.Z >= dimensions.Z) pwrapped.Z -= dimensions.Z;

					int dataPointIndex = pwrapped.Z * (dimensions.X * dimensions.Y) + pwrapped.Y * dimensions.X + pwrapped.X;
					CALIB_ASSERT(dataPointIndex >= 0 && dataPointIndex < data.size());

					CAFloat clippedVolume = projectCell(p, cell);

					DataPoint& pointData = data[dataPointIndex];
					pointData.volume += clippedVolume;
					pointData.deformationGradient += strainCalculator.getDeformationGradients()[tetIndex] * clippedVolume;
					pointData.plasticDeformationGradient += strainCalculator.getPlasticDeformationGradients()[tetIndex] * clippedVolume;
				}
			}
		}

		tetIndex++;
	}

	// Sum nodal values from all processors.
	// Only master processor will obtain the complete data.
	CALIB_ASSERT(sizeof(data.front()) == sizeof(CAFloat) * 19);
	if(parallel().isMaster() == false) {
#ifdef CALIB_USE_MPI
		MPI_Reduce(&data.front(), NULL, sizeof(data.front()) * data.size() / sizeof(CAFloat), MPI_FLOATTYPE, MPI_SUM, 0, context().communicator());
#endif
	}
	else {
		if(parallel().processorCount() != 1) {
			globalData.resize(data.size());
#ifdef CALIB_USE_MPI
			MPI_Reduce(&data.front(), &globalData.front(), sizeof(data.front()) * data.size() / sizeof(CAFloat), MPI_FLOATTYPE, MPI_SUM, 0, context().communicator());
#endif
		}
		else globalData.swap(data);

		// Normalize data point values.
		for(vector<DataPoint>::iterator d = globalData.begin(); d != globalData.end(); ++d) {
			if(d->volume != 0) {
				d->deformationGradient /= d->volume;
				d->plasticDeformationGradient /= d->volume;
			}
		}
	}

#if 0
	ofstream stream("clip_debug.vtk");
	stream << "# vtk DataFile Version 3.0" << endl;
	stream << "# Clipped tets" << endl;
	stream << "ASCII" << endl;
	stream << "DATASET UNSTRUCTURED_GRID" << endl;
	stream << "POINTS " << debugTets.size() << " double" << endl;
	for(vector<Point3>::const_iterator receivedPoint = debugTets.begin(); receivedPoint != debugTets.end(); ++receivedPoint)
		stream << " " << receivedPoint->X << " " << receivedPoint->Y << " " << receivedPoint->Z << endl;
	int numCells = debugTets.size()/4;
	stream << endl << "CELLS " << numCells << " " << (numCells * 5) << endl;
	for(int i = 0; i < numCells; i++)
		stream << "4 " << (i*4+0) << " " << (i*4+1) << " " << (i*4+2) << " " << (i*4+3) << endl;
	stream << endl << "CELL_TYPES " << numCells << endl;
	for(int i = 0; i < numCells; i++)
		stream << "10" << endl;
#endif

#if ANALYSIS_OUTPUT_LEVEL >= 2
	parallel().barrier();
	context().msgLogger() << "Projection time: " << projectionTimer.elapsedTime() << " sec." << endl;
#endif
}

/******************************************************************************
* Determine the grid point that is closest to the given world space point.
******************************************************************************/
Point3I StructuredGrid::absoluteToGrid(const Point3& p) const
{
	Vector3 rp = absoluteToGridTM * (p - absoluteToGridOffset);
	return Point3I((int)floor(rp.X), (int)floor(rp.Y), (int)floor(rp.Z));
}

/******************************************************************************
* Project the field values of a single tetrahedron cell onto one point of the structured grid.
******************************************************************************/
CAFloat StructuredGrid::projectCell(const Point3I& gridPoint, Tessellation::CellHandle cell)
{
	// Set up the clipping planes.
	Vector3 clipPlaneNormals[6];
	CAFloat clipPlaneDists[6];
	int numClipPlanes = 0;
	for(int dim = 0; dim < 3; dim++) {

		Vector3 normal = Normalize(CrossProduct(gridToAbsoluteTM.column((dim+1)%3), gridToAbsoluteTM.column((dim+2)%3)));
		if(DotProduct(normal, gridToAbsoluteTM.column(dim)) < 0.0)
			normal = -normal;

		if(gridPoint[dim] != 0 || structure.pbc(dim)) {
			clipPlaneNormals[numClipPlanes] = normal;
			clipPlaneDists[numClipPlanes] = DotProduct(normal, absoluteToGridOffset + gridToAbsoluteTM * Vector3(gridPoint.X, gridPoint.Y, gridPoint.Z) - ORIGIN);
			numClipPlanes++;
		}

		if(gridPoint[dim] != dimensions[dim]-1 || structure.pbc(dim)) {
			clipPlaneNormals[numClipPlanes] = -normal;
			clipPlaneDists[numClipPlanes] = -DotProduct(normal, absoluteToGridOffset + gridToAbsoluteTM * Vector3(gridPoint.X+1, gridPoint.Y+1, gridPoint.Z+1) - ORIGIN);
			numClipPlanes++;
		}
	}

	Point3 vertices[4];
	for(int v = 0; v < 4; v++)
		vertices[v] = cell->vertex(v)->point().atom->pos;

	return clipTetrahedron(vertices, clipPlaneNormals, clipPlaneDists, numClipPlanes);
}

/******************************************************************************
* Clips a tetrahedron at a single clipping plane.
******************************************************************************/
CAFloat StructuredGrid::clipTetrahedron(const Point3 vertices[4], const Vector3* clipPlaneNormals, const CAFloat* clipPlaneDists, int clipPlaneIndex)
{
	if(clipPlaneIndex == 0) {
		// Calculate volume of tetrahedron.
		return fabs(DotProduct(vertices[0] - vertices[3], CrossProduct(vertices[1] - vertices[3], vertices[2] - vertices[3])) / 6.0);
	}
	else {
		clipPlaneIndex--;
		// Clip tetrahedron at current plane.
		Point3 posVerts[4];
		Point3 negVerts[4];
		int numPositive = 0, numNegative = 0;
		CAFloat tpos[4];
		CAFloat tneg[4];
		for(int v = 0; v < 4; v++) {
			CAFloat t = DotProduct(vertices[v] - ORIGIN, clipPlaneNormals[clipPlaneIndex]) - clipPlaneDists[clipPlaneIndex];
			if(t > 0.0) {
				tpos[numPositive] =  t;
				posVerts[numPositive++] = vertices[v];
			}
			else {
				tneg[numNegative] =  t;
				negVerts[numNegative++] = vertices[v];
			}
		}
		if(numPositive == 0) return 0.0;
		else if(numPositive == 1) {
			Point3 newVertices[4];
			newVertices[3] = posVerts[0];
			for(int v = 0; v < 3; v++)
				newVertices[v] = posVerts[0] + (negVerts[v] - posVerts[0]) * (tpos[0] / (tpos[0] - tneg[v]));
			return clipTetrahedron(newVertices, clipPlaneNormals, clipPlaneDists, clipPlaneIndex);
		}
		else if(numPositive == 2) {
			Point3 i00 = posVerts[0] + (negVerts[0] - posVerts[0]) * (tpos[0] / (tpos[0] - tneg[0]));
			Point3 i01 = posVerts[0] + (negVerts[1] - posVerts[0]) * (tpos[0] / (tpos[0] - tneg[1]));
			Point3 i10 = posVerts[1] + (negVerts[0] - posVerts[1]) * (tpos[1] / (tpos[1] - tneg[0]));
			Point3 i11 = posVerts[1] + (negVerts[1] - posVerts[1]) * (tpos[1] / (tpos[1] - tneg[1]));
			CAFloat volumeSum = 0.0;
			Point3 newVertices[4];
			newVertices[0] = posVerts[0]; newVertices[1] = posVerts[1]; newVertices[2] = i11; newVertices[3] = i10;
			volumeSum += clipTetrahedron(newVertices, clipPlaneNormals, clipPlaneDists, clipPlaneIndex);
			newVertices[1] = i01;
			volumeSum += clipTetrahedron(newVertices, clipPlaneNormals, clipPlaneDists, clipPlaneIndex);
			newVertices[2] = i00;
			volumeSum += clipTetrahedron(newVertices, clipPlaneNormals, clipPlaneDists, clipPlaneIndex);
			return volumeSum;
		}
		else if(numPositive == 3) {
			Point3 i00 = posVerts[0] + (negVerts[0] - posVerts[0]) * (tpos[0] / (tpos[0] - tneg[0]));
			Point3 i10 = posVerts[1] + (negVerts[0] - posVerts[1]) * (tpos[1] / (tpos[1] - tneg[0]));
			Point3 i20 = posVerts[2] + (negVerts[0] - posVerts[2]) * (tpos[2] / (tpos[2] - tneg[0]));
			CAFloat volumeSum = 0.0;
			Point3 newVertices[4];
			newVertices[0] = posVerts[0]; newVertices[1] = i00; newVertices[2] = i10; newVertices[3] = i20;
			volumeSum += clipTetrahedron(newVertices, clipPlaneNormals, clipPlaneDists, clipPlaneIndex);
			newVertices[1] = posVerts[1];
			volumeSum += clipTetrahedron(newVertices, clipPlaneNormals, clipPlaneDists, clipPlaneIndex);
			newVertices[2] = posVerts[2];
			volumeSum += clipTetrahedron(newVertices, clipPlaneNormals, clipPlaneDists, clipPlaneIndex);
			return volumeSum;
		}
		else {
			return clipTetrahedron(vertices, clipPlaneNormals, clipPlaneDists, clipPlaneIndex);
		}
	}
}

/******************************************************************************
* Outputs the grid to a VTK file for visualization.
******************************************************************************/
void StructuredGrid::writeToVTKFile(ostream& stream) const
{
	Vector3I extdim = dimensions;
	if(structure.pbc(0)) extdim.X += 1;
	if(structure.pbc(1)) extdim.Y += 1;
	if(structure.pbc(2)) extdim.Z += 1;
	int numPoints = extdim.X * extdim.Y * extdim.Z;

	stream << "# vtk DataFile Version 3.0" << endl;
	stream << "# Grid" << endl;
	stream << "ASCII" << endl;

	stream << "DATASET STRUCTURED_POINTS" << endl;
	stream << "DIMENSIONS " << extdim.X << " " << extdim.Y << " " << extdim.Z << endl;
	stream << "ORIGIN " << structure.simulationCellOrigin().X << " " << structure.simulationCellOrigin().Y << " " << structure.simulationCellOrigin().Z << endl;
	stream << "SPACING " << Length(gridToAbsoluteTM.column(0)) << " " << Length(gridToAbsoluteTM.column(1)) << " " << Length(gridToAbsoluteTM.column(2)) << endl;

	stream << "POINT_DATA " << numPoints << endl;

	stream << "SCALARS volume double" << endl;
	stream << "LOOKUP_TABLE default" << endl;
	for(int z = 0; z < extdim.Z; z++) {
		for(int y = 0; y < extdim.Y; y++) {
			for(int x = 0; x < extdim.X; x++) {
				int pointIndex = (z % dimensions.Z) * dimensions.X * dimensions.Y + (y % dimensions.Y) * dimensions.X + (x % dimensions.X);
				stream << globalData[pointIndex].volume << endl;
			}
		}
	}

	stream << "SCALARS total_shear double" << endl;
	stream << "LOOKUP_TABLE default" << endl;
	for(int z = 0; z < extdim.Z; z++) {
		for(int y = 0; y < extdim.Y; y++) {
			for(int x = 0; x < extdim.X; x++) {
				int pointIndex = (z % dimensions.Z) * dimensions.X * dimensions.Y + (y % dimensions.Y) * dimensions.X + (x % dimensions.X);
				SymmetricTensor2 strain = (Product_AtA(globalData[pointIndex].deformationGradient) - IDENTITY) * 0.5;
				CAFloat shear = sqrt(square(strain(0,1)) + square(strain(1,2)) + square(strain(0,2)) + (square(strain(1,1) - strain(2,2)) + square(strain(0,0) - strain(2,2)) + square(strain(0,0) - strain(1,1))) / 6.0);
				CALIB_ASSERT(!isnan(shear) && !isinf(shear));
				stream << shear << endl;
			}
		}
	}

	stream << "SCALARS plastic_shear double" << endl;
	stream << "LOOKUP_TABLE default" << endl;
	for(int z = 0; z < extdim.Z; z++) {
		for(int y = 0; y < extdim.Y; y++) {
			for(int x = 0; x < extdim.X; x++) {
				int pointIndex = (z % dimensions.Z) * dimensions.X * dimensions.Y + (y % dimensions.Y) * dimensions.X + (x % dimensions.X);
				SymmetricTensor2 strain = (Product_AtA(globalData[pointIndex].plasticDeformationGradient) - IDENTITY) * 0.5;
				CAFloat shear = sqrt(square(strain(0,1)) + square(strain(1,2)) + square(strain(0,2)) + (square(strain(1,1) - strain(2,2)) + square(strain(0,0) - strain(2,2)) + square(strain(0,0) - strain(1,1))) / 6.0);
				CALIB_ASSERT(!isnan(shear) && !isinf(shear));
				stream << shear << endl;
			}
		}
	}

	stream << "SCALARS elastic_shear double" << endl;
	stream << "LOOKUP_TABLE default" << endl;
	for(int z = 0; z < extdim.Z; z++) {
		for(int y = 0; y < extdim.Y; y++) {
			for(int x = 0; x < extdim.X; x++) {
				int pointIndex = (z % dimensions.Z) * dimensions.X * dimensions.Y + (y % dimensions.Y) * dimensions.X + (x % dimensions.X);
				Matrix3 elasticF;
				if(fabs(globalData[pointIndex].plasticDeformationGradient.determinant()) <= CAFLOAT_EPSILON)
					elasticF = NULL_MATRIX;
				else
					elasticF = globalData[pointIndex].deformationGradient * CALIB_MATRIX_INVERSE(globalData[pointIndex].plasticDeformationGradient);
				SymmetricTensor2 strain = (Product_AtA(elasticF) - IDENTITY) * 0.5;
				CAFloat shear = sqrt(square(strain(0,1)) + square(strain(1,2)) + square(strain(0,2)) + (square(strain(1,1) - strain(2,2)) + square(strain(0,0) - strain(2,2)) + square(strain(0,0) - strain(1,1))) / 6.0);
				CALIB_ASSERT(!isnan(shear) && !isinf(shear));
				stream << shear << endl;
			}
		}
	}
}

}; // End of namespace
