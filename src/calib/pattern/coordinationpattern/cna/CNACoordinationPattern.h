///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_CNA_COORDINATION_PATTERN_H
#define __CA_CNA_COORDINATION_PATTERN_H

#include "../CoordinationPattern.h"

namespace CALib {

/// Pair of neighbor atoms that form a bond (bit-wise storage).
typedef unsigned int CNAPairBond;

/**
 * Describes a bond between the central atom and one of its neighbors.
 */
struct CNABond
{
	/// Number of common neighbors.
	int numCommonNeighbors;

	/// Number of bonds between common neighbors.
	int numBonds;

	/// Length of the longest continuous bond chain between common neighbors.
	int maxChainLength;

	/// Used for sorting of bonds.
	bool operator<(const CNABond& other) const {
		if(numCommonNeighbors < other.numCommonNeighbors) return true;
		if(numCommonNeighbors == other.numCommonNeighbors) {
			if(numBonds < other.numBonds) return true;
			if(numBonds == other.numBonds) {
				return maxChainLength < other.maxChainLength;
			}
		}
		return false;
	}

	/// Used for checking bond signatures.
	bool operator==(const CNABond& other) const {
		return numCommonNeighbors == other.numCommonNeighbors && numBonds == other.numBonds && maxChainLength == other.maxChainLength;
	}

	/// Used for checking bond signatures.
	bool operator!=(const CNABond& other) const {
		return numCommonNeighbors != other.numCommonNeighbors || numBonds != other.numBonds || maxChainLength != other.maxChainLength;
	}

};

/// Writes this CNA signature to a stream.
inline std::ostream& operator<<(std::ostream& stream, const CNABond& bond) {
	return stream << bond.numCommonNeighbors << " " << bond.numBonds << " " << bond.maxChainLength;
}

/// Loads the CNA signature from a stream.
inline std::istream& operator>>(std::istream& stream, CNABond& bond) {
	return stream >> bond.numCommonNeighbors >> bond.numBonds >> bond.maxChainLength;
}

/**
 * Stores a common neighbor analysis (CNA) pattern.
 */
struct CNACoordinationPattern : public CoordinationPattern
{
	/// Constructor.
	CNACoordinationPattern(Type _coordinationPatternType = COORDINATION_PATTERN_CNA) : CoordinationPattern(_coordinationPatternType) {}

	/// The CNA signatures for the neighbors of the central atom.
	CNABond cnaNeighbors[CA_MAX_PATTERN_NEIGHBORS];

	/// The sorted copy of the array above.
	CNABond cnaNeighborsSorted[CA_MAX_PATTERN_NEIGHBORS];

	/// Two-dimensional bit array that stores the bonds between neighbors.
	NeighborBondArray neighborArray;

	/// Computes the CNA signature for a neighbor bond.
	static void calculateCNASignature(const NeighborBondArray& neighborArray, int neighborIndex, CNABond& cnaBond, int nneighbors);

	/// Find all atoms that are nearest neighbors of the given pair of atoms.
	static int findCommonNeighbors(const NeighborBondArray& neighborArray, int atom2, unsigned int& commonNeighbors, int numNeighbors);

	/// Finds all bonds between common nearest neighbors.
	static int findNeighborBonds(const NeighborBondArray& neighborArray, unsigned int commonNeighbors, int numNeighbors, CNAPairBond* neighborBonds);

	/// Find all chains of bonds between common neighbors and determine the length
	///  of the longest continuous chain.
	static int calcMaxChainLength(CNAPairBond* neighborBonds, int numBonds);
};

}; // End of namespace

#endif // __CA_CNA_COORDINATION_PATTERN_H

