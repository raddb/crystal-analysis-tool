///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "NDACoordinationPatternMatcher.h"
#include "NDACoordinationPattern.h"
#include "../../../atomic_structure/AtomicStructure.h"

using namespace std;

namespace CALib {

bool NDACoordinationPatternMatcher::initializeNeighborPermutations()
{
	CALIB_ASSERT(atomIndex() >= 0 && atomIndex() < structure().numLocalAtoms());
	CALIB_ASSERT(neighborList().isSorted());
	const NDACoordinationPattern& ndaPattern = static_cast<const NDACoordinationPattern&>(pattern());

	// Check for sufficient number of neighbors.
	int nn = pattern().numNeighbors();
	CALIB_ASSERT(nn > 0);
	if(neighborList().neighborCount(atomIndex()) < nn)
		return false;

	// Determine scale factor that relates the actual distance distribution to the reference distribution.
	CAFloat localScale = 0;
	NeighborList::neighbor_iterator n = neighborList().neighbors(atomIndex()).begin();
	for(int ni = 0; ni < nn; ni++, ++n)
		localScale += sqrt(n->distsq) * pattern().neighbors[ni].scaleFactor;
	CAFloat scaling_factor_sq = square(1.0 / localScale);

	// Check if the (N+1)-th neighbor is far enough away from the central atom.
	if(neighborList().neighborCount(atomIndex()) > nn) {
		CAFloat localGap = neighborList().neighborDistance(atomIndex(), nn) - neighborList().neighborDistance(atomIndex(), nn - 1);
		if(localGap <= pattern().cutoffGap * localScale)
			return false;
	}

	// Calculate mutual neighbor distances.
	CAFloat bondLengthsSq[CA_MAX_PATTERN_NEIGHBORS * CA_MAX_PATTERN_NEIGHBORS];
	CAFloat* bondLengthsSqIter = bondLengthsSq;
	NeighborList::neighbor_iterator n1 = neighborList().neighbors(atomIndex()).begin();
	for(int ni1 = 0; ni1 < nn; ni1++, ++n1) {
		bondLengthsSquared[ni1][ni1] = 0;
		NeighborList::neighbor_iterator n2 = n1 + 1;
		for(int ni2 = ni1 + 1; ni2 < nn; ni2++, ++n2) {
			*bondLengthsSqIter++ = bondLengthsSquared[ni1][ni2] = bondLengthsSquared[ni2][ni1] =
					(n2->delta - n1->delta).squaredNorm() * scaling_factor_sq;
		}
	}

	// Make a quick check on the sorted bond length list.
	int numBonds = nn * (nn - 1) / 2;
	CALIB_ASSERT(numBonds == bondLengthsSqIter - bondLengthsSq);
	sort(bondLengthsSq, bondLengthsSqIter);
	for(int i = 0; i < numBonds; i++) {
		if(bondLengthsSq[i] < ndaPattern.bondRangeDistribution[i].minDistanceSquared || bondLengthsSq[i] > ndaPattern.bondRangeDistribution[i].maxDistanceSquared) {
			return false;
		}
	}

	// Reset permutation.
	_previousIndices.assign(nn, -1);
	_atomNeighborIndices.resize(nn);
	for(int n = 0; n < nn; n++)
		_atomNeighborIndices[n] = n;

	// Find first matching permutation.
	for(;;) {
		if(validatePermutation())
			return true;
		bitmapSort(_atomNeighborIndices.begin() + _validUpTo + 1, _atomNeighborIndices.end(), nn);
		if(!boost::next_permutation(_atomNeighborIndices))
			break;
	}

	return false;
}

bool NDACoordinationPatternMatcher::nextNeighborPermutation()
{
	for(;;) {
		if(!boost::next_permutation(_atomNeighborIndices))
			break;
		if(validatePermutation())
			return true;
		bitmapSort(_atomNeighborIndices.begin() + _validUpTo + 1, _atomNeighborIndices.end(), _atomNeighborIndices.size());
	}
	return false;
}

bool NDACoordinationPatternMatcher::validatePermutation()
{
	// Validate interconnectivity.
	bool check = false;
	const NDACoordinationPattern& ndaPattern = static_cast<const NDACoordinationPattern&>(pattern());
	for(int ni1 = 0; ni1 < ndaPattern.numNeighbors(); ni1++) {
		_validUpTo = ni1;
		int atomNeighborIndex1 = _atomNeighborIndices[ni1];
		if(atomNeighborIndex1 != _previousIndices[ni1]) check = true;
		_previousIndices[ni1] = atomNeighborIndex1;
		if(check) {
			for(int ni2 = 0; ni2 < ni1; ni2++) {
				int atomNeighborIndex2 = _atomNeighborIndices[ni2];
				CAFloat bondLengthSq = bondLengthsSquared[atomNeighborIndex1][atomNeighborIndex2];
				const NDABond& bond = ndaPattern.bonds[ni1][ni2];
				if(bondLengthSq < bond.minDistanceSquared || bondLengthSq > bond.maxDistanceSquared) {
					return false;
				}
			}
		}
	}

	_validUpTo = _atomNeighborIndices.size();
	return true;
}

}; // End of namespace
