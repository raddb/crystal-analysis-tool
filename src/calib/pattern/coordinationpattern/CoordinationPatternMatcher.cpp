///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "CoordinationPatternMatcher.h"
#include "nda/NDACoordinationPatternMatcher.h"
#include "cna/CNACoordinationPatternMatcher.h"
#include "acna/AdaptiveCNACoordinationPatternMatcher.h"
#include "diamond/DiamondCoordinationPatternMatcher.h"
#include "../../atomic_structure/AtomicStructure.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Creates the right pattern matcher for the given pattern.
******************************************************************************/
auto_ptr<CoordinationPatternMatcher> CoordinationPatternMatcher::createPatternMatcher(const AtomicStructure& structure, const NeighborList& neighborList, const CoordinationPattern& pattern)
{
	switch(pattern.coordinationPatternType) {
	case CoordinationPattern::COORDINATION_PATTERN_NDA: return auto_ptr<CoordinationPatternMatcher>(new NDACoordinationPatternMatcher(structure, neighborList, pattern));
	case CoordinationPattern::COORDINATION_PATTERN_CNA: return auto_ptr<CoordinationPatternMatcher>(new CNACoordinationPatternMatcher(structure, neighborList, pattern));
	case CoordinationPattern::COORDINATION_PATTERN_ACNA: return auto_ptr<CoordinationPatternMatcher>(new AdaptiveCNACoordinationPatternMatcher(structure, neighborList, pattern));
	case CoordinationPattern::COORDINATION_PATTERN_DIAMOND: return auto_ptr<CoordinationPatternMatcher>(new DiamondCoordinationPatternMatcher(structure, neighborList, pattern));
	default: CALIB_ASSERT(false); return auto_ptr<CoordinationPatternMatcher>();
	}
}

/******************************************************************************
* Constructor
******************************************************************************/
CoordinationPatternMatcher::CoordinationPatternMatcher(const AtomicStructure& structure, const NeighborList& neighborList, const CoordinationPattern& pattern) :
		_structure(structure), _neighborList(neighborList), _pattern(pattern)
{
	_numMatches = 0;
	_numAtomsMatched = 0;
	_numTrialPermutations = 0;
	_atomIndex = -1;
	_cutoff = neighborList.cutoff();
}

/******************************************************************************
* Finds the next occurrence of the pattern.
******************************************************************************/
bool CoordinationPatternMatcher::findNextPermutation(int atomIndex)
{
	if(_atomIndex != atomIndex) {
		_atomIndex = atomIndex;
		if(initializeNeighborPermutations()) {
			_numAtomsMatched++;
			_numMatches++;
			return true;
		}
		else {
			_atomIndex = -1;
			return false;
		}
	}
	else {
		if(nextNeighborPermutation()) {
			_numMatches++;
			return true;
		}
		else {
			_atomIndex = -1;
			return false;
		}
	}
}

}; // End of namespace
