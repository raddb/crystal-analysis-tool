///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_SIMULATION_CELL_H
#define __CA_SIMULATION_CELL_H

#include "../CALib.h"
#include "../context/CAContext.h"

namespace CALib {

/**
 * This class stores the geometry of the simulation cell.
 *
 * It is sub-classed by AtomicStructure, which stores the atomic coordinates.
 */
class SimulationCell : public ContextReference, private boost::noncopyable
{
public:

	/// Constructor that takes a reference to the global context object.
	SimulationCell(CAContext& context);

	/************************************ General properties ***************************************/

	/// Returns the simulation timestep at which the snapshot of the atomic structure was taken.
	/// The timestep number is read from the input file header (if provided by the input file's format).
	int timestep() const { return _timestep; }

	/// Sets the simulation timestep.
	/// The timestep number will be written in the header of the output file (if supported by the file format).
	void setTimestep(int t) { _timestep = t; }

	/********************************* Simulation cell properties **************************************/

	/// Returns periodic boundary flags for the simulation cell.
	const boost::array<bool,3>& pbcFlags() const { return _pbcFlags; }

	/// Returns whether periodic boundary conditions are used in the given dimension.
	bool pbc(int dim) const { return _pbcFlags[dim]; }

	/// Returns whether the simulation cell has any periodic boundary conditions applied.
	bool hasPeriodicBoundaries() const { return _pbcFlags[0] || _pbcFlags[1] || _pbcFlags[2]; }

	/// Returns global simulation cell matrix.
	/// This matrix maps the unit cube ([0,1]^3) to the simulation cell.
	inline const AffineTransformation& simulationCellMatrix() const { return _simulationCell; }

	/// Returns inverse of the global simulation cell matrix.
	/// This matrix maps the simulation cell to the unit cube ([0,1]^3).
	inline const AffineTransformation& reciprocalSimulationCellMatrix() const { return _reciprocalSimulationCell; }

	/// Returns an edge vector of the simulation cell.
	inline AffineTransformation::MatrixType::ConstColXpr cellVector(int dim) const { return _simulationCell.matrix().col(dim); }

	/// Computes the (positive) volume of the simulation cell.
	inline CAFloat cellVolume() const {
		return fabs(_simulationCell.linear().determinant());
	}

	/// Calculates the normal vector of the given simulation cell side.
	inline Vector3 cellNormalVector(int dim) const {
		Vector3 normal = cellVector((dim+1)%3).cross(cellVector((dim+2)%3));
		// Flip normal if necessary.
		if(normal.dot(cellVector(dim)) < 0.0)
			return normal / (-normal.norm());
		else
			return normal.normalized();
	}

	/// Sets the simulation cell geometry and boundary conditions.
	void setSimulationCell(const AffineTransformation& cellMatrix, const boost::array<bool,3>& pbc);

	/// Sets up the simulation cell and MPI domain decomposition.
	void setupSimulationCellAndDecomposition(AffineTransformation simulationCell, boost::array<bool,3> pbc, bool broadcastInfo = true);

	/********************************** Spatial decomposition***************************************/

	/// Returns the shape matrix of the local processor domain.
	const AffineTransformation& processorDomainMatrix() const { return _processorDomain; }

	/************************************ Vector operations *****************************************/

	/// Wraps a vector that connects two atoms such that it never points from one side of the
	///       simulation box to the opposite side. The wrapping is required if periodic boundary
	///       conditions are used.
	/// \param v The input vector.
	/// \return The wrapped vector. It is equal to the input vector if both atoms lie on the same side of the
	///         simulation box. If the distance of the atoms is more than half the box size then the vector
	///         is wrapped to make it smaller than this threshold value.
	inline Vector3 wrapVector(const Vector3& v) const {
		Vector3 result = v;
		for(int dim = 0; dim < 3; dim++) {
			if(pbc(dim)) {
				// Transform vector to reduced cell coordinates.
				CAFloat rv = _reciprocalSimulationCell.linear().row(dim) * v;
				while(rv > CAFloat(+0.5)) { rv -= CAFloat(1); result -= cellVector(dim); }
				while(rv < CAFloat(-0.5)) { rv += CAFloat(1); result += cellVector(dim); }
			}
		}
		return result;
	}

	/// Wraps a vector in reduced coordinates that connects two atoms such that it never points from one side of the
	///       simulation box to the opposite side.
	/// \param rv The input vector.
	/// \return The wrapped vector. It is equal to the input vector if both atoms lie on the same side of the
	///         simulation box. If the distance of the atoms is more than half the box size then the vector
	///         is wrapped to make it smaller than this threshold value.
	Vector3 wrapReducedVector(Vector3 rv) const {
		for(int dim = 0; dim < 3; dim++) {
			if(pbc(dim)) {
				while(rv[dim] > CAFloat(+0.5)) { rv[dim] -= CAFloat(1); }
				while(rv[dim] < CAFloat(-0.5)) { rv[dim] += CAFloat(1); }
			}
		}
		return rv;
	}

	/// Calculates the shift vector that must be subtracted from point B to bring it close to point A such that
	/// the vector (B-A) is not a wrapped vector.
	Vector3 calculateShiftVector(const Vector3& a, const Vector3& b) const;

	/// Determines whether the given vector is so long such that it would
	/// be wrapped at periodic boundaries (according to the minimum image convention).
	bool isWrappedVector(const Vector3& v) const;

	/// Determines whether the given vector (in reduced coordinates) is so long such that it would
	/// be wrapped at periodic boundaries (according to the minimum image convention).
	bool isReducedWrappedVector(const Vector3& v) const;

	/// Determines the periodic image of the simulation in which the given point is located.
	Vector3I periodicImage(const Vector3& p) const;

	/// Wraps a point to be inside the simulation cell. The wrapping is required if periodic boundary
	///       conditions are used.
	/// \param p The input point.
	/// \return The wrapped point.
	template<typename Derived>
	inline Vector3 wrapPoint(const Eigen::MatrixBase<Derived>& p) const {
		Vector3 result = p;
		for(int dim = 0; dim < 3; dim++) {
			if(pbc(dim)) {
				// Transform point to reduced cell coordinates.
				CAFloat rp = _reciprocalSimulationCell.linear().row(dim) * p + _reciprocalSimulationCell.translation()[dim];
				while(rp >= +1.0) { rp -= CAFloat(1); result -= cellVector(dim); }
				while(rp <  0.0 ) { rp += CAFloat(1); result += cellVector(dim); }
			}
		}
		return result;
	}

	/// Wraps a point in reduced coordinated to be inside the simulation cell. The wrapping is required if periodic boundary
	///       conditions are used.
	/// \param p The input point.
	/// \return The wrapped point.
	Vector3 wrapReducedPoint(Vector3 p) const {
		for(int dim = 0; dim < 3; dim++) {
			if(pbc(dim)) {
				while(p[dim] >= CAFloat(1)) { p[dim] -= CAFloat(1); }
				while(p[dim] <  CAFloat(0)) { p[dim] += CAFloat(1); }
			}
		}
		return p;
	}

	/// Converts a point from reduced cell coordinates to absolute coordinates.
	template<typename Derived>
	inline Vector3 reducedToAbsolutePoint(const Eigen::MatrixBase<Derived>& reducedPoint) const { return _simulationCell * reducedPoint; }

	/// Converts a point from absolute coordinates to reduced cell coordinates.
	template<typename Derived>
	inline Vector3 absoluteToReducedPoint(const Eigen::MatrixBase<Derived>& worldPoint) const { return _reciprocalSimulationCell * worldPoint; }

	/// Converts a vector from reduced cell coordinates to absolute coordinates.
	template<typename Derived>
	inline Vector3 reducedToAbsoluteVector(const Eigen::MatrixBase<Derived>& reducedVec) const { return _simulationCell.linear() * reducedVec; }

	/// Converts a vector from absolute coordinates to a vector in reduced cell coordinates.
	template<typename Derived>
	inline Vector3 absoluteToReducedVector(const Eigen::MatrixBase<Derived>& worldVec) const { return _reciprocalSimulationCell.linear() * worldVec; }

	/// Determines the processor domain that contains the given world-space point.
	Vector3I absoluteToProcessorGrid(const Vector3& pos, bool clamp = true) const;

	/// Converts a position in the processor grid to an absolute coordinate.
	/// No wrapping is performed.
	Vector3 processorGridToAbsolute(const Vector3I& pos) const;

protected:

	/// The frame/timestep number of the current simulation snapshot.
	/// This is read from the header of the input simulation file.
	int _timestep;

	/// Indicates whether periodic boundary conditions are enabled for each of the three spatial directions.
	boost::array<bool,3> _pbcFlags;

	/// The global simulation cell matrix.
	AffineTransformation _simulationCell;
	/// The inverse of the simulation cell matrix used to transform absolute coordinates to reduced coordinates.
	AffineTransformation _reciprocalSimulationCell;
	/// Transforms absolute coordinates to processor grid coordinates.
	AffineTransformation _absoluteToProcGridTM;
	/// Transforms processor grid coordinates to absolute coordinates.
	AffineTransformation _procGridToAbsoluteTM;

	/// The adjusted simulation cell matrix that includes all atoms.
	/// This may be larger than the original simulation cell if atoms are positioned slightly
	/// outside of the original cell (in non-periodic directions).
	AffineTransformation _boundingBox;

	/// The shape matrix of the local processor domain.
	AffineTransformation _processorDomain;
};

}; // End of namespace

#endif // __CA_SIMULATION_CELL_H
