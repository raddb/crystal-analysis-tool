///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_DISLOCATION_NETWORK_H
#define __CA_DISLOCATION_NETWORK_H

#include "../CALib.h"
#include "InterfaceMesh.h"
#include "../cluster_graph/ClusterGraph.h"
#include "BurgersCircuit.h"

namespace CALib {

struct DislocationSegment;

/**
 * A start or end node of a dislocation segment.
 *
 * Each segment has two nodes that mark the beginning and the end of the segment.
 */
struct DislocationNode
{
	/// The dislocation segment to which this node belongs.
	DislocationSegment* segment;

	/// The opposite node associated with the dislocation segment.
	DislocationNode* oppositeNode;

	/// Pointer to the next node in linked list of nodes that form a junction.
	/// If this node is not part of a junction, then this pointer points to the node itself.
	DislocationNode* junctionRing;

	/// The Burgers circuit associated with this node.
	/// This field is only used during dislocation line tracing.
	BurgersCircuit* circuit;

	/// Constructor.
	DislocationNode() : circuit(NULL) { junctionRing = this; }

	/// Returns the (signed) Burgers vector of the node.
	/// This is the Burgers vector of the segment if this node is a forward node,
	/// or the negative Burgers vector if this node is a backward node.
	inline ClusterVector burgersVector() const;

	/// Returns the position of the node by looking up the coordinates of the
	/// start or end point of the dislocation segment to which the node belongs.
	inline const Vector3& position() const;

	/// Returns true if this node is the forward node of its segment, that is,
	/// when it is at the end of the associated dislocation segment.
	inline bool isForwardNode() const;

	/// Returns true if this node is the backward node of its segment, that is,
	/// when it is at the beginning of the associated dislocation segment.
	inline bool isBackwardNode() const;

	/// Determines whether the given node forms a junction with the given node.
	bool formsJunctionWith(DislocationNode* other) const {
		DislocationNode* n = this->junctionRing;
		do {
			if(other == n) return true;
			n = n->junctionRing;
		}
		while(n != this->junctionRing);
		return false;
	}

	/// Makes two nodes part of a junction.
	/// If any of the two nodes were already part of a junction, then
	/// a single junction is created that encompasses all nodes.
	void connectNodes(DislocationNode* other) {
		CALIB_ASSERT(!other->formsJunctionWith(this));
		CALIB_ASSERT(!this->formsJunctionWith(other));

		DislocationNode* tempStorage = junctionRing;
		junctionRing = other->junctionRing;
		other->junctionRing = tempStorage;

		CALIB_ASSERT(other->formsJunctionWith(this));
		CALIB_ASSERT(this->formsJunctionWith(other));
	}

	/// If this node is part of a junction, dissolves the junction.
	/// The nodes of all junction arms will become dangling nodes.
	void dissolveJunction() {
		DislocationNode* n = this->junctionRing;
		while(n != this) {
			DislocationNode* next = n->junctionRing;
			n->junctionRing = n;
			n = next;
		}
		this->junctionRing = this;
	}

	/// Counts the number of arms belonging to the junction.
	int countJunctionArms() const {
		int armCount = 1;
		for(DislocationNode* armNode = this->junctionRing; armNode != this; armNode = armNode->junctionRing)
			armCount++;
		return armCount;
	}

	/// Return whether the end of a segment, represented by this node, does not merge into a junction.
	bool isDangling() const {
		CALIB_ASSERT(circuit == NULL || (junctionRing == this) == circuit->isDangling);
		return (junctionRing == this);
	}
};

/**
 * A dislocation segment.
 *
 * Each segment has a Burgers vector and consists of a piecewise-linear curve in space.
 *
 * Two dislocation nodes delimit the segment.
 */
struct DislocationSegment
{
	/// The unique identifier of the dislocation segment.
	int id;

	/// The piecewise linear curve in space.
	std::deque<Vector3> line;

	/// Stores the circumference of the dislocation core at every sampling point along the line.
	/// This information is used to coarsen the sampling point array adaptively since a large
	/// core size leads to a high sampling rate.
	std::deque<int> coreSize;

	/// The Burgers vector of the dislocation segment. It is expressed in the coordinate system of
	/// the crystal cluster which the segment is embedded in.
	ClusterVector burgersVector;

	/// The two nodes that delimit the segment.
	DislocationNode* nodes[2];

	/// The segment that replaces this discarded segment if the two have been merged into one segment.
	DislocationSegment* replacedWith;

	/// Constructs a new dislocation segment with the given Burgers vector
	/// and connecting the two dislocation nodes.
	DislocationSegment(const ClusterVector& b, DislocationNode* forwardNode, DislocationNode* backwardNode) :
		burgersVector(b), replacedWith(NULL) {
		nodes[0] = forwardNode;
		nodes[1] = backwardNode;
		forwardNode->segment = this;
		backwardNode->segment = this;
		forwardNode->oppositeNode = backwardNode;
		backwardNode->oppositeNode = forwardNode;
	}

	/// Returns the forward-pointing node at the end of the dislocation segment.
	DislocationNode& forwardNode() const { return *nodes[0]; }

	/// Returns the backward-pointing node at the start of the dislocation segment.
	DislocationNode& backwardNode() const { return *nodes[1]; }

	/// Returns true if this segment forms a closed loop, that is, when its two nodes form a single 2-junction.
	/// Note that an infinite dislocation line, passing through a periodic boundary, is also considered a loop.
	bool isClosedLoop() const {
		CALIB_ASSERT(nodes[0] && nodes[1]);
		return (nodes[0]->junctionRing == nodes[1]) && (nodes[1]->junctionRing == nodes[0]);
	}

	/// Returns true if this segment is an infinite dislocation line passing through a periodic boundary.
	/// A segment is considered infinite if it is a closed loop but its start and end points do not coincide.
	bool isInfiniteLine() const {
		return isClosedLoop() && (line.back() - line.front()).isZero(CA_ATOM_VECTOR_EPSILON) == false;
	}

	/// Calculates the line length of the segment.
	CAFloat calculateLength() const {
		CALIB_ASSERT(!isDegenerate());

		CAFloat length = 0.0;
		std::deque<Vector3>::const_iterator i1 = line.begin();
		for(;;) {
			std::deque<Vector3>::const_iterator i2 = i1 + 1;
			if(i2 == line.end()) break;
			length += (*i1 - *i2).norm();
			i1 = i2;
		}
		return length;
	}

	/// Returns true if this segment's curve consists of less than two points.
	bool isDegenerate() const { return line.size() <= 1; }

	/// Finds the best cluster in which to express the segment's true Burgers vector.
	void findOptimalBurgersVectorRepresentation();

	/// Reverses the direction of the segment.
	void flipOrientation() {
		burgersVector = -burgersVector;
		std::swap(nodes[0], nodes[1]);
		std::reverse(line.begin(), line.end());
		std::reverse(coreSize.begin(), coreSize.end());
	}
};

/// Returns true if this node is the forward node of its segment, that is,
/// when it is at the end of the associated dislocation segment.
inline bool DislocationNode::isForwardNode() const
{
	return &segment->forwardNode() == this;
}

/// Returns true if this node is the backward node of its segment, that is,
/// when it is at the beginning of the associated dislocation segment.
inline bool DislocationNode::isBackwardNode() const
{
	return &segment->backwardNode() == this;
}

/// Returns the (signed) Burgers vector of the node.
/// This is the Burgers vector of the segment if this node is a forward node,
/// or the negative Burgers vector if this node is a backward node.
inline ClusterVector DislocationNode::burgersVector() const
{
	if(isForwardNode())
		return segment->burgersVector;
	else
		return -segment->burgersVector;
}

/// Returns the position of the node by looking up the coordinates of the
/// start or end point of the dislocation segment to which the node belongs.
inline const Vector3& DislocationNode::position() const
{
	if(isForwardNode())
		return segment->line.back();
	else
		return segment->line.front();
}

/**
 * This class holds the entire network of dislocation segments.
 */
class DislocationNetwork : public ContextReference
{
public:

	/// Constructor.
	DislocationNetwork(const ClusterGraph& clusterGraph) : ContextReference(clusterGraph.context()), _simulationCell(clusterGraph.structure()),
		_clusterGraph(clusterGraph) {}

	/// Returns a const-reference to the cluster graph.
	const ClusterGraph& clusterGraph() const { return _clusterGraph; }

	/// Returns a const-reference to the simulation cell.
	const SimulationCell& simulationCell() const { return _simulationCell; }

	/// Returns the list of dislocation segments.
	const std::vector<DislocationSegment*>& segments() const { return _segments; }

	/// Allocates a new dislocation segment terminated by two nodes.
	DislocationSegment* createSegment(const ClusterVector& burgersVector);

	/// Removes a segment from the global list of segments.
	void discardSegment(DislocationSegment* segment);

	/// Coarsen the sampling of dislocation lines for better visualization results.
	void coarsenDislocationSegments(CAFloat linePointInterval);

	/// Smoothes the dislocation lines for better visualization results.
	void smoothDislocationSegments(int smoothingLevel);

	/// Wraps the dislocation lines at periodic boundaries.
	void wrapDislocationSegments();

	/// Tries to align the orientation of dislocation segments.
	void alignDislocationDirections();

private:

	/// The simulation cell.
	const SimulationCell& _simulationCell;

	/// The cluster graph.
	const ClusterGraph& _clusterGraph;

	// Used to allocate memory for DislocationNode instances.
	MemoryPool<DislocationNode> _nodePool;

	/// The list of dislocation segments.
	std::vector<DislocationSegment*> _segments;

	/// Used to allocate memory for DislocationSegment objects.
	MemoryPool<DislocationSegment> _segmentPool;
};

}; // End of namespace

#endif // __CA_DISLOCATION_NETWORK_H
