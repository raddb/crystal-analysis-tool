///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "DislocationNetwork.h"
#include "../context/CAContext.h"
#include "../atomic_structure/AtomicStructure.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Checks if the given floating point number is integer.
******************************************************************************/
static bool isInteger(CAFloat v, int& intPart)
{
	static const CAFloat epsilon = 1e-3f;
	CAFloat ip;
	CAFloat frac = std::modf(v, &ip);
	if(frac >= -epsilon && frac <= epsilon) intPart = (int)ip;
	else if(frac >= CAFloat(1)-epsilon) intPart = (int)ip + 1;
	else if(frac <= CAFloat(-1)+epsilon) intPart = (int)ip - 1;
	else return false;
	return true;
}

/******************************************************************************
* Computes a rating for the given Burgers vector.
******************************************************************************/
static pair<int,int> rateBurgersVectorRepresentation(const ClusterVector& b)
{
	if(b.cluster()->pattern->isLattice() == false)
		return make_pair(numeric_limits<int>::min(), b.cluster()->atomCount);
	if(b.cluster()->pattern->burgersVectorFamilyId(b.localVec()) != 0)
		return make_pair(numeric_limits<int>::max(), b.cluster()->atomCount);

	CAFloat smallestCompnt = CAFLOAT_MAX;
	for(int i = 0; i < 3; i++) {
		CAFloat c = fabs(b.localVec()[i]);
		if(c < smallestCompnt && c > CAFloat(1e-3))
			smallestCompnt = c;
	}
	if(smallestCompnt != CAFLOAT_MAX) {
		CAFloat m = CAFloat(1) / smallestCompnt;
		for(int f = 1; f <= 11; f++) {
			int multiplier;
			if(!isInteger(m*f, multiplier))
				continue;
			Vector3 bm = b.localVec() * multiplier;
			Vector3I bmi;
			if(isInteger(bm.x(),bmi.x()) && isInteger(bm.y(),bmi.y()) && isInteger(bm.z(),bmi.z())) {
				return make_pair(numeric_limits<int>::max() - multiplier, b.cluster()->atomCount);
			}
		}
	}

	return make_pair(0, b.cluster()->atomCount);
}

/******************************************************************************
* Finds the best cluster in which to express the segment's true Burgers vector.
******************************************************************************/
void DislocationSegment::findOptimalBurgersVectorRepresentation()
{
	pair<int,int> bestRating(std::numeric_limits<int>::min(),std::numeric_limits<int>::min());

	for(int nodeIndex = 0; nodeIndex < 2; nodeIndex++) {
		// Iterate over all possible start edges of the circuit.
		BurgersCircuit* circuit = nodes[nodeIndex]->circuit;
		InterfaceMesh::Edge* startEdge = circuit->firstEdge;
		do {

			// Compute Burgers vector from circuit.
			Vector3 b(Vector3::Zero());
			Matrix3 tm(Matrix3::Identity());
			InterfaceMesh::Edge* edge = startEdge;
			do {
				CALIB_ASSERT(edge != NULL);
				b += tm * edge->clusterVector;
				tm = tm * edge->clusterTransition->reverse->tm;
				edge = edge->nextCircuitEdge;
			}
			while(edge != startEdge);
			ClusterVector burgersVector(b, startEdge->clusterTransition->cluster1);

			// Express Burgers vector in frame of parent cluster.
			ClusterVector parentBurgersVector(burgersVector);
			if(burgersVector.cluster()->isRoot() == false)
				parentBurgersVector = ClusterVector(burgersVector.cluster()->parentTransition->transform(burgersVector.localVec()), burgersVector.cluster()->parentCluster());

			// Rate Burgers vector representation.
			pair<int,int> rating = rateBurgersVectorRepresentation(parentBurgersVector);
			if(rating >= bestRating) {
				this->burgersVector = (nodeIndex == 0) ? parentBurgersVector : -parentBurgersVector;
				bestRating = rating;
			}

			startEdge = startEdge->nextCircuitEdge;
		}
		while(startEdge != circuit->firstEdge);
	}
	CALIB_ASSERT(this->burgersVector.cluster() != NULL);

	if(this->burgersVector.cluster()->pattern->isLattice() == false) {
		for(ClusterTransition* t1 = this->burgersVector.cluster()->transitions; t1 != NULL; t1 = t1->next) {
			if(t1->distance > 2) continue;
			Cluster* c1 = t1->cluster2;
			if(c1->pattern->isLattice()) {
				ClusterVector bt(t1->transform(this->burgersVector.localVec()), c1);
				ClusterVector btParent(c1->parentTransition->transform(bt.localVec()), c1->parentCluster());
				pair<int,int> rating = rateBurgersVectorRepresentation(btParent);
				if(rating > bestRating) {
					this->burgersVector = btParent;
					bestRating = rating;
				}
			}
		}
	}
}

/******************************************************************************
* Allocates a new dislocation segment terminated by two nodes.
******************************************************************************/
DislocationSegment* DislocationNetwork::createSegment(const ClusterVector& burgersVector)
{
	DislocationNode* forwardNode = _nodePool.construct();
	DislocationNode* backwardNode = _nodePool.construct();

	DislocationSegment* segment = _segmentPool.construct(burgersVector, forwardNode, backwardNode);
	segment->id = _segments.size();
	_segments.push_back(segment);

	return segment;
}

/******************************************************************************
* Removes a segment from the global list of segments.
******************************************************************************/
void DislocationNetwork::discardSegment(DislocationSegment* segment)
{
	CALIB_ASSERT(segment != NULL);
	std::vector<DislocationSegment*>::iterator i = boost::find(_segments, segment);
	CALIB_ASSERT(i != _segments.end());
	_segments.erase(i);
}

/******************************************************************************
* Coarsen the dislocation lines for better visualization results.
******************************************************************************/
void DislocationNetwork::coarsenDislocationSegments(CAFloat linePointInterval)
{
	// Reduce resolution of the dislocation line segments.
	// Remove points from the line segments since usually we have too many of them.
	for(int segmentIndex = 0; segmentIndex < segments().size(); segmentIndex++) {
		DislocationSegment& segment = *_segments[segmentIndex];
		CALIB_ASSERT(segment.line.size() >= 2);
		CALIB_ASSERT(segment.coreSize.size() == segment.line.size());

		if(segment.line.size() <= 2)
			continue;		// Nothing to do when segment has only two vertices. Cannot be coarsened any further.

		// We should retain at least four points including the end points to obtain a line with proper curvature.

		int inputPtr = 0;
		int outputPtr = 0;
		int sum;
		int count;

		sum = count = 0;
		do {
			sum += segment.coreSize[inputPtr];
			count++;
			inputPtr++;
		}
		while(2*count*count < (int)(linePointInterval * sum) && count < segment.line.size()/4);
		segment.coreSize[outputPtr] = sum / count;
		outputPtr++;

		do {
			sum = count = 0;
			Vector3 com(Vector3::Zero());
			do {
				sum += segment.coreSize[inputPtr];
				com += segment.line[inputPtr];
				count++;
				inputPtr++;
			}
			while(count*count < (int)(linePointInterval * sum) && count < segment.line.size()/4 && inputPtr < segment.line.size()-1);
			segment.line[outputPtr] = com / count;
			segment.coreSize[outputPtr] = sum / count;
			outputPtr++;
		}
		while(inputPtr < segment.line.size() - 1);

		sum = count = 0;
		do {
			sum += segment.coreSize[inputPtr];
			count++;
			inputPtr++;
		}
		while(inputPtr < segment.line.size());
		segment.line[outputPtr] = segment.line.back();
		segment.coreSize[outputPtr] = sum / count;
		outputPtr++;

		segment.line.resize(outputPtr);
		segment.coreSize.resize(outputPtr);

		CALIB_ASSERT(segment.line.size() >= 2);
		CALIB_ASSERT(!segment.isClosedLoop() || segment.isInfiniteLine() || segment.line.size() >= 3);
	}
}

/******************************************************************************
* Smoothes the dislocation lines for better visualization results.
******************************************************************************/
void DislocationNetwork::smoothDislocationSegments(int smoothingLevel)
{
	if(smoothingLevel <= 0)
		return;	// Nothing to do.

	// This is the 2d implementation of the mesh smoothing algorithm:
	//
	// Gabriel Taubin
	// A Signal Processing Approach To Fair Surface Design
	// In SIGGRAPH 95 Conference Proceedings, pages 351-358 (1995)

	CAFloat k_PB = 0.1;
	CAFloat lambda = 0.5;
	CAFloat mu = 1.0 / (k_PB - 1.0/lambda);
	const CAFloat prefactors[2] = { lambda, mu };

	for(int segmentIndex = 0; segmentIndex < segments().size(); segmentIndex++) {
		DislocationSegment& segment = *_segments[segmentIndex];
		deque<Vector3>& line = segment.line;
		CALIB_ASSERT(line.size() >= 2);
		if(line.size() <= 2)
			continue;	// Nothing to do.

		vector<Vector3> laplacians(line.size());
		for(int iteration = 0; iteration < smoothingLevel; iteration++) {

			for(int pass = 0; pass <= 1; pass++) {
				// Compute discrete Laplacian for each point.
				vector<Vector3>::iterator l = laplacians.begin();
				if(segment.isClosedLoop() == false)
					(*l++).setZero();
				else
					(*l++) = ((*(line.end()-2) - *(line.end()-3)) + (*(line.begin()+1) - line.front())) * 0.5;

				deque<Vector3>::const_iterator p1 = line.begin();
				deque<Vector3>::const_iterator p2 = p1 + 1;
				for(;;) {
					deque<Vector3>::const_iterator p0 = p1;
					++p1;
					++p2;
					if(p2 == line.end())
						break;
					*l++ = ((*p0 - *p1) + (*p2 - *p1)) * 0.5;
				}
				*l++ = laplacians.front();
				CALIB_ASSERT(l == laplacians.end());

				vector<Vector3>::const_iterator lc = laplacians.begin();
				for(deque<Vector3>::iterator p = line.begin(); p != line.end(); ++p, ++lc) {
					*p += prefactors[pass] * (*lc);
				}
			}
		}
	}
}

/******************************************************************************
* Wraps the dislocation lines at periodic boundaries.
******************************************************************************/
void DislocationNetwork::wrapDislocationSegments()
{
	if(!simulationCell().hasPeriodicBoundaries())
		return;	// Nothing to do.

	size_t oldSegmentCount = segments().size();
	for(size_t segmentIndex = 0; segmentIndex < oldSegmentCount; segmentIndex++) {
		DislocationSegment* segment = segments()[segmentIndex];
		deque<Vector3>& line = segment->line;
		CALIB_ASSERT(line.size() >= 2);

		deque<Vector3>::const_iterator p1 = line.begin();
		Vector3 p1reduced = simulationCell().absoluteToReducedPoint(*p1);
		Vector3 p1reducedi;
		p1reducedi.x() = simulationCell().pbc(0) ? floor(p1reduced.x()) : 0;
		p1reducedi.y() = simulationCell().pbc(1) ? floor(p1reduced.y()) : 0;
		p1reducedi.z() = simulationCell().pbc(2) ? floor(p1reduced.z()) : 0;
		Vector3 p1wrapped = *p1 - simulationCell().reducedToAbsoluteVector(p1reducedi);

		deque<Vector3> outputLine;
		outputLine.push_back(p1wrapped);
		deque<Vector3>* output1 = &outputLine;

		for(deque<Vector3>::const_iterator p2 = p1 + 1; p2 != line.end(); ++p2) {
			Vector3 p2reduced = simulationCell().absoluteToReducedPoint(*p2);
			Vector3 p2reducedi;
			p2reducedi.x() = simulationCell().pbc(0) ? floor(p2reduced.x()) : 0;
			p2reducedi.y() = simulationCell().pbc(1) ? floor(p2reduced.y()) : 0;
			p2reducedi.z() = simulationCell().pbc(2) ? floor(p2reduced.z()) : 0;
			Vector3 p2wrapped = *p2 - simulationCell().reducedToAbsoluteVector(p2reducedi);

			if(p2reducedi != p1reducedi) {
				DislocationSegment* newSegment = createSegment(segment->burgersVector);
				swap(newSegment->nodes[1], segment->nodes[1]);
				newSegment->nodes[1]->segment = newSegment;
				newSegment->nodes[1]->oppositeNode = newSegment->nodes[0];
				newSegment->nodes[0]->oppositeNode = newSegment->nodes[1];
				segment->nodes[1]->segment = segment;
				segment->nodes[1]->oppositeNode = segment->nodes[0];
				segment->nodes[0]->oppositeNode = segment->nodes[1];
				newSegment->id = segment->id;
				deque<Vector3>* output2 = &newSegment->line;
				if(p2reducedi.x() == p1reducedi.x() && p2reducedi.y() == p1reducedi.y()) {
					output1->push_back(p1wrapped - ((p1reduced.z() - max(p1reducedi.z(), p2reducedi.z())) / (p2reduced.z() - p1reduced.z())) * (*p2 - *p1));
					output2->push_back(p2wrapped - ((p2reduced.z() - max(p1reducedi.z(), p2reducedi.z())) / (p2reduced.z() - p1reduced.z())) * (*p2 - *p1));
				}
				else if(p2reducedi.z() == p1reducedi.z() && p2reducedi.y() == p1reducedi.y()) {
					output1->push_back(p1wrapped - ((p1reduced.x() - max(p1reducedi.x(), p2reducedi.x())) / (p2reduced.x() - p1reduced.x())) * (*p2 - *p1));
					output2->push_back(p2wrapped - ((p2reduced.x() - max(p1reducedi.x(), p2reducedi.x())) / (p2reduced.x() - p1reduced.x())) * (*p2 - *p1));
				}
				else if(p2reducedi.x() == p1reducedi.x() && p2reducedi.z() == p1reducedi.z()) {
					output1->push_back(p1wrapped - ((p1reduced.y() - max(p1reducedi.y(), p2reducedi.y())) / (p2reduced.y() - p1reduced.y())) * (*p2 - *p1));
					output2->push_back(p2wrapped - ((p2reduced.y() - max(p1reducedi.y(), p2reducedi.y())) / (p2reduced.y() - p1reduced.y())) * (*p2 - *p1));
				}
				output1 = output2;
			}
			output1->push_back(p2wrapped);

			p1 = p2;
			p1reduced = p2reduced;
			p1reducedi = p2reducedi;
			p1wrapped = p2wrapped;
		}
		segment->line.swap(outputLine);
	}

	// Clean up, remove degenerate segments from list.
	_segments.erase(std::remove_if(_segments.begin(), _segments.end(),
			mem_fun(&DislocationSegment::isDegenerate)), _segments.end());
}

/******************************************************************************
* Tries to align the orientation of dislocation segments.
******************************************************************************/
void DislocationNetwork::alignDislocationDirections()
{
	for(int segmentIndex = 0; segmentIndex < segments().size(); segmentIndex++) {
		DislocationSegment& segment = *_segments[segmentIndex];
		deque<Vector3>& line = segment.line;
		CALIB_ASSERT(line.size() >= 2);
		if(line.size() <= 2)
			continue;	// Nothing to do.

		Vector3 dir = line.back() - line.front();

		if(dir.isZero(CA_ATOM_VECTOR_EPSILON))
			continue;

		if(std::abs(dir.x()) > std::abs(dir.y())) {
			if(std::abs(dir.x()) > std::abs(dir.z())) {
				if(dir.x() >= 0.0) continue;
			}
			else {
				if(dir.z() >= 0.0) continue;
			}
		}
		else {
			if(std::abs(dir.y()) > std::abs(dir.z())) {
				if(dir.y() >= 0.0) continue;
			}
			else {
				if(dir.z() >= 0.0) continue;
			}
		}

		segment.flipOrientation();
	}
}


}; // End of namespace
