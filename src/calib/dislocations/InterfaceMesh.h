///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_INTERFACE_MESH_H
#define __CA_INTERFACE_MESH_H

#include "../CALib.h"
#include "../util/MemoryPool.h"
#include "../context/CAContext.h"
#include "../cluster_graph/Cluster.h"
#include "../cluster_graph/ClusterVector.h"
#include "../tessellation/ElasticMapping.h"

namespace CALib {

struct BurgersCircuit;
struct TriangleMeshVertex;
struct TriangleMeshEdge;
struct BurgersCircuitSearchStruct;

/**
 * This class builds the interface mesh that separates the 'bad' crystal regions
 * from the 'good' crystal regions.
 */
class InterfaceMesh : public ContextReference
{
public:

	struct Node;
	struct Facet;

	/**
	 * A half-edge connecting two nodes of the interface mesh.
	 */
	struct Edge
	{
		/// The second node of the half edge.
		Node* _node2;

		/// The (unwrapped) vector connecting the two vertex atoms of the edge.
		Vector3 physicalVector;

		/// The vector in the reference configuration corresponding to this edge.
		Vector3 clusterVector;

		/// The cluster transition when going from the cluster of node 1 to the cluster of node 2.
		ClusterTransition* clusterTransition;

		/// The incident facet of this half edge.
		Facet* facet;

		/// The opposite half edge.
		Edge* oppositeEdge;

		/// The next edge in the linked list of edges leaving the node.
		Edge* nextNodeEdge;

		/// The Burgers circuit this edge is part of.
		/// This field is used by the DislocationTracer class.
		BurgersCircuit* circuit;

		/// If this edge is part of a Burgers circuit then this points to the next edge in the circuit.
		/// This field is used by the DislocationTracer class.
		Edge* nextCircuitEdge;

		/// The output edge created for this interface mesh edge in the output mesh.
		/// This field is used by the TriangleMesh class.
		TriangleMeshEdge* outputEdge;

		/// Returns the first node of the half edge.
		Node* node1() const {
			CALIB_ASSERT(oppositeEdge != NULL);
			return oppositeEdge->node2();
		}

		/// Returns the second  node of the half edge.
		Node* node2() const { return _node2; }
	};

	/**
	 * A node of the interface mesh.
	 */
	struct Node
	{
		enum NodeBitFlags {
			IS_GHOST_NODE,		/// Indicates that the node has been derived from a ghost atom.
			VISITED,			/// Indicates that the node has been visited.
			INVALID,			/// Indicates that the node is invalid and should be skipped during Burgers circuit search.
			NUM_FLAGS
		};

		/// Constructor.
		Node(const Vector3& _pos) : pos(_pos), edges(NULL), burgersSearchStruct(NULL), outputVertex(NULL) {}

		/// The world-space position of the node. This is copied from
		/// the atom from which this node was derived.
		Vector3 pos;

		/// The unique identifier of the node. This is copied from
		/// the atom from which this node was derived.
		AtomInteger tag;

		/// The flags array.
		std::bitset<NUM_FLAGS> flags;

		/// The index of the node in the InterfaceMesh's array of mesh nodes.
		int index;

		/// The linked list of half edges leaving this node.
		Edge* edges;

		/// This pointer is used during Burgers circuit search on the mesh.
		/// This field is used by the DislocationTracer class.
		BurgersCircuitSearchStruct* burgersSearchStruct;

		/// The output vertex created for this mesh node in the output mesh.
		/// This field is used by the TriangleMesh class.
		TriangleMeshVertex* outputVertex;
	};

	/**
	 * A triangle of the interface mesh.
	 */
	struct Facet
	{
		enum FacetBitFlags {
			IS_GHOST_FACET,			/// Indicates that the facet is not inside the local processor domain.
			IS_PRIMARY_SEGMENT, 	/// Indicates that the facet is part of a primary dislocation segment.
			NUM_FLAGS
		};

		/// Default constructor.
		Facet() : circuit(NULL) {}

		/// The three edges that bound this triangle facet.
		Edge* edges[3];

		/// The Burgers circuit which has swept this facet.
		/// This field is used by the DislocationTracer class.
		BurgersCircuit* circuit;

		/// Bit flags of this facet.
		std::bitset<NUM_FLAGS> flags;

		/// Returns true if the given node is among the three vertices of this triangle facet.
		bool hasVertex(Node* v) const {
			return edges[0]->node2() == v || edges[1]->node2() == v || edges[2]->node2() == v;
		}

		/// Returns the vertex node of the facet with the given index.
		Node* vertex(int index) const {
			CALIB_ASSERT(index >= 0 && index < 3);
			if(index == 0) return edges[2]->node2();
			else if(index == 1) return edges[0]->node2();
			else return edges[1]->node2();
		}

		/// Returns the index of the given vertex node.
		int vertexIndex(Node* v) const {
			if(edges[0]->node2() == v) return 1;
			else if(edges[1]->node2() == v) return 2;
			else {
				CALIB_ASSERT(edges[2]->node2() == v);
				return 0;
			}
		}

		/// Returns the index of the given edge in this facet's list of edges.
		int edgeIndex(Edge* e) const {
			if(edges[0] == e) return 0;
			else if(edges[1] == e) return 1;
			else {
				CALIB_ASSERT(edges[2] == e);
				return 2;
			}
		}

		/// Returns the edge following the given edge in this facet's list of edges.
		Edge* nextEdge(Edge* e) const {
			if(edges[0] == e) return edges[1];
			else if(edges[1] == e) return edges[2];
			else {
				CALIB_ASSERT(edges[2] == e);
				return edges[0];
			}
		}

		/// Returns the edge preceding the given edge in this facet's list of edges.
		Edge* previousEdge(Edge* e) const {
			if(edges[0] == e) return edges[2];
			else if(edges[1] == e) return edges[0];
			else {
				CALIB_ASSERT(edges[2] == e);
				return edges[1];
			}
		}
	};

public:

	/// Constructor.
	InterfaceMesh(const ElasticMapping& elasticMapping) : ContextReference(elasticMapping.context()),
		_elasticMapping(elasticMapping),
		_numLocalNodes(0), _numLocalFacets(0) {}

	/// Returns the mapping from the physical configuration of the system
	/// to the stress-free imaginary configuration.
	const ElasticMapping& elasticMapping() const { return _elasticMapping; }

	/// Returns the underlying tessellation of the atomistic system.
	const DelaunayTessellation& tessellation() const { return _elasticMapping.tessellation(); }

	/// Returns a reference to the underlying atomic structure.
	const AtomicStructure& structure() const { return _elasticMapping.structure(); }

	/// Returns a const-reference to the underlying cluster graph.
	const ClusterGraph& clusterGraph() const { return _elasticMapping.clusterGraph(); }

	/// Returns a reference to the underlying cluster graph.
	ClusterGraph& clusterGraph() { return const_cast<ClusterGraph&>(_elasticMapping.clusterGraph()); }

	/// Builds the interface mesh (only local part of processor).
	void build();

	/// Duplicates mesh nodes which are part of multiple manifolds.
	void finalizeMesh() {
		duplicateSharedMeshNodes();
	}

	/// Performs a thorough check of the topology of the interface mesh.
	/// Raises an error if something wrong is found. Note that this should never happen.
	void validate(bool performManifoldCheck = true) const;

	/// Merges partial meshes from all processors into a single mesh.
	void mergeParallel(InterfaceMesh& outputMesh);

	/// Returns the list of mesh nodes.
	const std::vector<Node*>& nodes() const { return _nodes; }

	/// Returns the list of mesh facets.
	const std::vector<Facet*>& facets() const { return _facets; }

private:

	/**
	 * Encapsulates a single tetrahedron of the Delaunay tessellation.
	 */
	struct Tetrahedron {

		/// Pointer to the original tetrahedron cell of the Delaunay tessellation.
		DelaunayTessellation::CellHandle cell;

		/// Indicates whether this tetrahedron belongs to the good or the bad crystal region.
		bool isGood;

		/// Pointers to the interface mesh facets associated with the four faces of the tetrahedron.
		/// Is only used if the cell is at the boundary of the bad region.
		boost::array<Facet*, 4> meshFacets;

		/// The vertex atoms of the tetrahedron. They are sorted according to their tags.
		boost::array<int, 4> vertexAtoms;
	};

	/// Generates the list of tetrahedra in the tessellation and classifies each one
	/// as being either good or bad.
	void classifyTetrahedra();

	/// Creates the mesh facets separating good and bad tetrahedra.
	void createSeparatingFacets();

	// Links half-edges to opposite half-edges.
	void linkHalfEdges();

	/// Duplicates mesh nodes which are part of multiple manifolds.
	size_t duplicateSharedMeshNodes();

	/// Determines whether the triangular facet specified by the three nodes is not owned by
	/// the local processor.
	static bool isGhostMeshFacet(Node* facetNodes[3]);

	/// Adds a new node to the mesh.
	Node* createNode(AtomInteger tag, const Vector3& pos, bool isGhostNode);

	/// Adds a new triangle facet to the mesh.
	Facet* createFacet(bool isGhostFacet);

	/// Adds a new edge to the mesh.
	Edge* createHalfEdge(Node* A, Node* B, const Vector3& edgeVector, ClusterTransition* clusterTransition);

private:

	/// The underlying mapping from the physical configuration of the system
	/// to the stress-free imaginary configuration.
	const ElasticMapping& _elasticMapping;

	/// The list of tetrahedra of the Delaunay tessellation.
	std::vector<Tetrahedron> _tetrahedra;

	/// The mesh edges.
	MemoryPool<Edge> _edgePool;

	/// The mesh nodes.
	std::vector<Node*> _nodes;

	/// The memory pool to create mesh nodes.
	MemoryPool<Node> _nodePool;

	/// The number of local nodes (which are not ghost nodes).
	int _numLocalNodes;

	/// The mesh facets.
	std::vector<Facet*> _facets;
	MemoryPool<Facet> _facetPool;

	/// The number of local facets, which are located inside the processor domain.
	int _numLocalFacets;
};

}; // End of namespace

#endif // __CA_INTERFACE_MESH_H
