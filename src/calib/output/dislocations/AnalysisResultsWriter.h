///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_ANALYSIS_RESULTS_WRITER_H
#define __CA_ANALYSIS_RESULTS_WRITER_H

#include "../../CALib.h"
#include "../../context/CAContext.h"
#include "../../dislocations/DislocationNetwork.h"
#include "../../trianglemesh/TriangleMesh.h"

namespace CALib {

/**
 * Writes the extracted dislocation lines and the defect surface to an output file that can be opened with OVITO.
 */
class AnalysisResultsWriter : public ContextReference
{
public:

	/// Constructor.
	AnalysisResultsWriter(CAContext& context) : ContextReference(context) {}

	/// Outputs the analysis results to a file.
	void writeAnalysisFile(std::ostream& stream, const DislocationNetwork& network, const std::vector<SuperPattern const*>& superPatterns, const TriangleMesh& surface, const std::string& outputFilename, const std::string& atomsFilename);

#if 0
	/// Outputs the analysis results to a file.
	void writeAnalysisFile(FILE* stream, const DislocationNetwork& network, const std::vector<SuperPattern const*>& superPatterns, const TriangleMesh& surface, const std::string& outputFilename, const std::string& atomsFilename);
#endif
};

}; // End of namespace

#endif // __CA_ANALYSIS_RESULTS_WRITER_H
