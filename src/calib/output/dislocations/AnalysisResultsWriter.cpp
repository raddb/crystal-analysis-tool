///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "AnalysisResultsWriter.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Outputs the analysis results to a file.
******************************************************************************/
void AnalysisResultsWriter::writeAnalysisFile(std::ostream& stream, const DislocationNetwork& network, const std::vector<SuperPattern const*>& superPatterns, const TriangleMesh& surface, const std::string& outputFilename, const std::string& atomsFilename)
{
	if(parallel().isMaster()) {
		// Write file header.
		stream << "CA_FILE_VERSION " << CA_OUTPUT_FILEFORMAT_VERSION << endl;
		stream << "CA_LIB_VERSION "<< CA_LIB_VERSION_STRING << endl;
		stream << setprecision(10);

		// Store path to this output file.
		stream << "OUTPUT_PATH " << outputFilename << endl;
		// Store path to the atoms file.
		stream << "ATOMS_PATH " << atomsFilename << endl;

		static const char* structureTypeStrings[] = {
				"POINTDEFECT",		// Zero-dimensional crystal defect.
				"UNDEFINED",
				"INTERFACE",		// Two-dimensional coherent crystal interface, grain boundary, or stacking fault.
				"LATTICE"			// Three-dimensional crystal lattice.
		};

		// Write list of super patterns.
		stream << "STRUCTURE_PATTERNS " << superPatterns.size() << endl;
		BOOST_FOREACH(const SuperPattern* pattern, superPatterns) {
			CALIB_ASSERT(pattern->periodicity >= 0 && pattern->periodicity < sizeof(structureTypeStrings)/sizeof(structureTypeStrings[0]));
			stream << "PATTERN ID " << pattern->index << endl;
			stream << "NAME " << pattern->name << endl;
			stream << "FULL_NAME " << pattern->fullName << endl;
			stream << "TYPE " << structureTypeStrings[pattern->periodicity] << endl;
			stream << "COLOR " << pattern->color.x() << " " << pattern->color.y() << " " << pattern->color.z() << endl;
			stream << "BURGERS_VECTOR_FAMILIES " << pattern->burgersVectorFamilies.size() << endl;
			BOOST_FOREACH(const BurgersVectorFamily& family, pattern->burgersVectorFamilies) {
				stream << "BURGERS_VECTOR_FAMILY ID " << family.id << endl;
				stream << family.name << endl;
				stream << family.burgersVector.x() << " " << family.burgersVector.y() << " " << family.burgersVector.z() << endl;
				stream << family.color.x() << " " << family.color.y() << " " << family.color.z() << endl;
			}
			stream << "END_PATTERN" << endl;
		}

		// Write simulation cell geometry to file.
		stream << "SIMULATION_CELL_ORIGIN "
				<< network.simulationCell().cellVector(3).x() << " " << network.simulationCell().cellVector(3).y() << " " << network.simulationCell().cellVector(3).z() << endl;
		stream << "SIMULATION_CELL "
				<< network.simulationCell().cellVector(0).x() << " " << network.simulationCell().cellVector(1).x() << " " << network.simulationCell().cellVector(2).x() << " "
				<< network.simulationCell().cellVector(0).y() << " " << network.simulationCell().cellVector(1).y() << " " << network.simulationCell().cellVector(2).y() << " "
				<< network.simulationCell().cellVector(0).z() << " " << network.simulationCell().cellVector(1).z() << " " << network.simulationCell().cellVector(2).z() << endl;
		stream << "PBC_FLAGS "
				<< (int)network.simulationCell().pbc(0) << " "
				<< (int)network.simulationCell().pbc(1) << " "
				<< (int)network.simulationCell().pbc(2) << endl;

		// Count root clusters.
		size_t numClusters = 0;
		const ClusterGraph& graph = network.clusterGraph();
		BOOST_FOREACH(Cluster* cluster, graph.clusters()) {
			if(cluster->isRoot()) {
				CALIB_ASSERT(cluster->outputId == numClusters);
				numClusters++;
			}
			else CALIB_ASSERT(cluster->outputId == -1);
		}

		// Serialize list of clusters.
		stream << "CLUSTERS " << numClusters << endl;
		BOOST_FOREACH(Cluster* cluster, graph.clusters()) {
			if(!cluster->isRoot()) continue;
			stream << "CLUSTER" << endl;
			stream << cluster->id.id << " " << cluster->id.processor << endl;
			stream << cluster->pattern->index << endl;
			stream << cluster->atomCount << endl;
			stream << cluster->centerOfMass.x() << " " << cluster->centerOfMass.y() << " " << cluster->centerOfMass.z() << endl;
			stream << cluster->orientation.col(0).x() << " " << cluster->orientation.col(1).x() << " " << cluster->orientation.col(2).x() << " "
					<< cluster->orientation.col(0).y() << " " << cluster->orientation.col(1).y() << " " << cluster->orientation.col(2).y() << " "
					<< cluster->orientation.col(0).z() << " " << cluster->orientation.col(1).z() << " " << cluster->orientation.col(2).z() << endl;
		}

		// Count cluster transitions.
		size_t numClusterTransitions = 0;
		BOOST_FOREACH(ClusterTransition* t, graph.clusterTransitions()) {
			if(t->cluster1->isRoot() && t->cluster2->isRoot())
				numClusterTransitions++;
		}

		// Serialize cluster transitions.
		stream << "CLUSTER_TRANSITIONS " << numClusterTransitions << endl;
		BOOST_FOREACH(ClusterTransition* t, graph.clusterTransitions()) {
			if(t->cluster1->isRoot() && t->cluster2->isRoot()) {
				stream << "TRANSITION " << t->cluster1->outputId << " " << t->cluster2->outputId << endl;
				stream << t->tm.col(0).x() << " " << t->tm.col(1).x() << " " << t->tm.col(2).x() << " "
						<< t->tm.col(0).y() << " " << t->tm.col(1).y() << " " << t->tm.col(2).y() << " "
						<< t->tm.col(0).z() << " " << t->tm.col(1).z() << " " << t->tm.col(2).z() << endl;
			}
		}

		// Serialize list of dislocation segments.
		stream << "DISLOCATIONS " << network.segments().size() << endl;
		BOOST_FOREACH(DislocationSegment* segment, network.segments()) {

			CALIB_ASSERT(segment->replacedWith == NULL);

			// Make sure proper index identifiers have been assigned to segments.
			CALIB_ASSERT(segment->id >= 0 && segment->id < network.segments().size());
			CALIB_ASSERT(network.segments()[segment->id] == segment);

			stream << segment->id << endl;

			stream << segment->burgersVector.localVec().x() << " " << segment->burgersVector.localVec().y() << " " << segment->burgersVector.localVec().z() << endl;
			CALIB_ASSERT(segment->burgersVector.cluster() != NULL);
			CALIB_ASSERT(segment->burgersVector.cluster()->isRoot());
			stream << segment->burgersVector.cluster()->outputId << endl;

			// Serialize polyline.
			stream << segment->line.size() << endl;
			BOOST_FOREACH(const Vector3& p, segment->line)
				stream << p.x() << " " << p.y() << " " << p.z() << endl;

			// Serialize dislocation core size.
			BOOST_FOREACH(int s, segment->coreSize)
				stream << s << endl;
		}

		// Store dislocation junction information.
		stream << "DISLOCATION_JUNCTIONS" << endl;
		BOOST_FOREACH(DislocationSegment* segment, network.segments()) {
			CALIB_ASSERT(segment->forwardNode().junctionRing->segment->id < network.segments().size());
			CALIB_ASSERT(segment->backwardNode().junctionRing->segment->id < network.segments().size());

			for(int nodeIndex = 0; nodeIndex < 2; nodeIndex++) {
				DislocationNode* otherNode = segment->nodes[nodeIndex]->junctionRing;
				bool isForward = otherNode->isForwardNode();
				stream << (int)isForward << " " << otherNode->segment->id << endl;
			}
		}

#ifdef DEBUG_CRYSTAL_ANALYSIS
		surface.validate();
#endif

		// Serialize list of vertices.
		stream << "DEFECT_MESH_VERTICES " << surface.vertices().size() << endl;
		BOOST_FOREACH(TriangleMeshVertex* vertex, surface.vertices()) {

			// Make sure indices have been assigned to vertices.
			CALIB_ASSERT(vertex->index >= 0 && vertex->index < surface.vertices().size());
			CALIB_ASSERT(surface.vertices()[vertex->index] == vertex);

			stream << vertex->pos.x() << " " << vertex->pos.y() << " " << vertex->pos.z() << endl;
		}

		// Serialize list of facets.
		stream << "DEFECT_MESH_FACETS " << surface.facets().size() << endl;
		int facetIndex = 0;
		BOOST_FOREACH(TriangleMeshFacet* facet, surface.facets()) {
			facet->index = facetIndex++;
			for(int v = 0; v < 3; v++)
				stream << facet->edges[v]->vertex1()->index << " ";
			stream << endl;
		}

		// Serialize facet adjacency information.
		BOOST_FOREACH(TriangleMeshFacet* facet, surface.facets()) {
			for(int v = 0; v < 3; v++) {
				CALIB_ASSERT(facet->edges[v]->oppositeEdge->facet != NULL);
				stream << facet->edges[v]->oppositeEdge->facet->index << " ";
			}
			stream << endl;
		}
	}
}

#if 0
/******************************************************************************
* Outputs the analysis results to a file.
******************************************************************************/
void AnalysisResultsWriter::writeAnalysisFile(FILE* stream, const DislocationNetwork& network, const std::vector<SuperPattern const*>& superPatterns, const TriangleMesh& surface, const std::string& outputFilename, const std::string& atomsFilename)
{
	if(parallel().isMaster()) {
		// Write file header.
		fprintf(stream, "CA_FILE_VERSION %i\n", CA_OUTPUT_FILEFORMAT_VERSION);
		fprintf(stream, "CA_LIB_VERSION %s\n", CA_LIB_VERSION_STRING);

		// Store path to this output file.
		fprintf(stream, "OUTPUT_PATH %s\n", outputFilename.c_str());
		// Store path to the atoms file.
		fprintf(stream, "ATOMS_PATH %s\n", atomsFilename.c_str());

		static const char* structureTypeStrings[] = {
				"POINTDEFECT",		// Zero-dimensional crystal defect.
				"UNDEFINED",
				"INTERFACE",		// Two-dimensional coherent crystal interface, grain boundary, or stacking fault.
				"LATTICE"			// Three-dimensional crystal lattice.
		};

		// Write list of super patterns.
		fprintf(stream, "STRUCTURE_PATTERNS %i\n", (int)superPatterns.size());
		BOOST_FOREACH(const SuperPattern* pattern, superPatterns) {
			CALIB_ASSERT(pattern->periodicity >= 0 && pattern->periodicity < sizeof(structureTypeStrings)/sizeof(structureTypeStrings[0]));
			fprintf(stream, "PATTERN ID %i\n", pattern->index);
			fprintf(stream, "NAME %s\n", pattern->name.c_str());
			fprintf(stream, "FULL_NAME %s\n", pattern->fullName.c_str());
			fprintf(stream, "TYPE %s\n", structureTypeStrings[pattern->periodicity]);
			fprintf(stream, "COLOR %f %f %f\n", pattern->color.x(), pattern->color.y(), pattern->color.z());
			fprintf(stream, "BURGERS_VECTOR_FAMILIES %i\n", (int)pattern->burgersVectorFamilies.size());
			BOOST_FOREACH(const BurgersVectorFamily& family, pattern->burgersVectorFamilies) {
				fprintf(stream, "BURGERS_VECTOR_FAMILY ID %i\n", family.id);
				fprintf(stream, "%s\n", family.name.c_str());
				fprintf(stream, "%.16f %.16f %.16f\n", family.burgersVector.x(), family.burgersVector.y(), family.burgersVector.z());
				fprintf(stream, "%f %f %f\n", family.color.x(), family.color.y(), family.color.z());
			}
			fprintf(stream, "END_PATTERN\n");
		}

		// Write simulation cell geometry to file.
		fprintf(stream, "SIMULATION_CELL_ORIGIN %f %f %f\n",
				network.simulationCell().cellVector(3).x(), network.simulationCell().cellVector(3).y(), network.simulationCell().cellVector(3).z());
		fprintf(stream, "SIMULATION_CELL %f %f %f %f %f %f %f %f %f\n",
				network.simulationCell().cellVector(0).x(), network.simulationCell().cellVector(1).x(), network.simulationCell().cellVector(2).x(),
				network.simulationCell().cellVector(0).y(), network.simulationCell().cellVector(1).y(), network.simulationCell().cellVector(2).y(),
				network.simulationCell().cellVector(0).z(), network.simulationCell().cellVector(1).z(), network.simulationCell().cellVector(2).z());
		fprintf(stream, "PBC_FLAGS %i %i %i\n",
				(int)network.simulationCell().pbc(0),
				(int)network.simulationCell().pbc(1),
				(int)network.simulationCell().pbc(2));

		// Count root clusters.
		size_t numClusters = 0;
		const ClusterGraph& graph = network.clusterGraph();
		BOOST_FOREACH(Cluster* cluster, graph.clusters()) {
			if(cluster->isRoot()) {
				CALIB_ASSERT(cluster->outputId == numClusters);
				numClusters++;
			}
			else CALIB_ASSERT(cluster->outputId == -1);
		}

		// Serialize list of clusters.
		fprintf(stream, "CLUSTERS %i\n", (int)numClusters);
		BOOST_FOREACH(Cluster* cluster, graph.clusters()) {
			if(!cluster->isRoot()) continue;
			fprintf(stream, "CLUSTER\n");
			fprintf(stream, "%i %i\n", cluster->id.id, cluster->id.processor);
			fprintf(stream, "%i\n", cluster->pattern->index);
			fprintf(stream, "%i\n", cluster->atomCount);
			fprintf(stream, "%f %f %f\n", cluster->centerOfMass.x(), cluster->centerOfMass.y(), cluster->centerOfMass.z());
			fprintf(stream, "%.10f %.10f %.10f %.10f %.10f %.10f %.10f %.10f %.10f\n",
					cluster->orientation.col(0).x(), cluster->orientation.col(1).x(), cluster->orientation.col(2).x(),
					cluster->orientation.col(0).y(), cluster->orientation.col(1).y(), cluster->orientation.col(2).y(),
					cluster->orientation.col(0).z(), cluster->orientation.col(1).z(), cluster->orientation.col(2).z());
		}

		// Count cluster transitions.
		size_t numClusterTransitions = 0;
		BOOST_FOREACH(ClusterTransition* t, graph.clusterTransitions()) {
			if(t->cluster1->isRoot() && t->cluster2->isRoot())
				numClusterTransitions++;
		}

		// Serialize cluster transitions.
		fprintf(stream, "CLUSTER_TRANSITIONS %i\n", (int)numClusterTransitions);
		BOOST_FOREACH(ClusterTransition* t, graph.clusterTransitions()) {
			if(t->cluster1->isRoot() && t->cluster2->isRoot()) {
				fprintf(stream, "TRANSITION %i %i\n", t->cluster1->outputId, t->cluster2->outputId);
				fprintf(stream, "%.10f %.10f %.10f %.10f %.10f %.10f %.10f %.10f %.10f\n",
						t->tm.col(0).x(), t->tm.col(1).x(), t->tm.col(2).x(),
						t->tm.col(0).y(), t->tm.col(1).y(), t->tm.col(2).y(),
						t->tm.col(0).z(), t->tm.col(1).z(), t->tm.col(2).z());
			}
		}

		// Serialize list of dislocation segments.
		fprintf(stream, "DISLOCATIONS %i\n", (int)network.segments().size());
		BOOST_FOREACH(DislocationSegment* segment, network.segments()) {

			CALIB_ASSERT(segment->replacedWith == NULL);

			// Make sure proper index identifiers have been assigned to segments.
			CALIB_ASSERT(segment->id >= 0 && segment->id < network.segments().size());
			CALIB_ASSERT(network.segments()[segment->id] == segment);

			fprintf(stream, "%i\n", segment->id);

			fprintf(stream, "%.10f %.10f %.10f\n", segment->burgersVector.localVec().x(), segment->burgersVector.localVec().y(), segment->burgersVector.localVec().z());
			CALIB_ASSERT(segment->burgersVector.cluster() != NULL);
			CALIB_ASSERT(segment->burgersVector.cluster()->isRoot());
			fprintf(stream, "%i\n", segment->burgersVector.cluster()->outputId);

			// Serialize polyline.
			fprintf(stream, "%i\n", (int)segment->line.size());
			BOOST_FOREACH(const Vector3& p, segment->line)
				fprintf(stream, "%f %f %f\n", p.x(), p.y(), p.z());

			// Serialize dislocation core size.
			BOOST_FOREACH(int s, segment->coreSize)
				fprintf(stream, "%i\n", s);
		}

		// Store dislocation junction information.
		fprintf(stream, "DISLOCATION_JUNCTIONS\n");
		BOOST_FOREACH(DislocationSegment* segment, network.segments()) {
			CALIB_ASSERT(segment->forwardNode().junctionRing->segment->id < network.segments().size());
			CALIB_ASSERT(segment->backwardNode().junctionRing->segment->id < network.segments().size());

			for(int nodeIndex = 0; nodeIndex < 2; nodeIndex++) {
				DislocationNode* otherNode = segment->nodes[nodeIndex]->junctionRing;
				bool isForward = otherNode->isForwardNode();
				fprintf(stream, "%i %i\n", (int)isForward, otherNode->segment->id);
			}
		}

#ifdef DEBUG_CRYSTAL_ANALYSIS
		surface.validate();
#endif

		// Serialize list of vertices.
		fprintf(stream, "DEFECT_MESH_VERTICES %i\n", (int)surface.vertices().size());
		BOOST_FOREACH(TriangleMeshVertex* vertex, surface.vertices()) {

			// Make sure indices have been assigned to vertices.
			CALIB_ASSERT(vertex->index >= 0 && vertex->index < surface.vertices().size());
			CALIB_ASSERT(surface.vertices()[vertex->index] == vertex);

			fprintf(stream, "%f %f %f\n", vertex->pos.x(), vertex->pos.y(), vertex->pos.z());
		}

		// Serialize list of facets.
		fprintf(stream, "DEFECT_MESH_FACETS %i\n", (int)surface.facets().size());
		int facetIndex = 0;
		BOOST_FOREACH(TriangleMeshFacet* facet, surface.facets()) {
			facet->index = facetIndex++;
			for(int v = 0; v < 3; v++)
				fprintf(stream, "%i ", facet->edges[v]->vertex1()->index);
			fprintf(stream, "\n");
		}

		// Serialize facet adjacency information.
		BOOST_FOREACH(TriangleMeshFacet* facet, surface.facets()) {
			for(int v = 0; v < 3; v++) {
				CALIB_ASSERT(facet->edges[v]->oppositeEdge->facet != NULL);
				fprintf(stream, "%i ", facet->edges[v]->oppositeEdge->facet->index);
			}
			fprintf(stream, "\n");
		}
	}
}
#endif

}; // End of namespace
