///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_CLUSTER_GRAPH_WRITER_H
#define __CA_CLUSTER_GRAPH_WRITER_H

#include "../../CALib.h"
#include "../../context/CAContext.h"

namespace CALib {

class ClusterGraph;

/**
 * Writes the cluster graph to an output file.
 */
class ClusterGraphWriter : public ContextReference
{
public:

	/// Constructor.
	ClusterGraphWriter(CAContext& context) : ContextReference(context) {}

	/// Outputs the cluster graph to a VTK file.
	void writeVTKFile(std::ostream& stream, const ClusterGraph& graph);
};

}; // End of namespace

#endif // __CA_CLUSTER_GRAPH_WRITER_H
