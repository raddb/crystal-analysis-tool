///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_ELASTIC_FIELD_WRITER_H
#define __CA_ELASTIC_FIELD_WRITER_H

#include "../../CALib.h"
#include "FieldWriter.h"
#include "../../context/CAContext.h"
#include "../../deformation/ElasticField.h"

namespace CALib {

/**
 * Writes the computed elastic deformation gradient field to an output file for visualization.
 */
class ElasticFieldWriter : public FieldWriter
{
public:

	enum FieldComponents {
		CELL_VOLUME,
		CELL_ASPECT_RATIO,
		F_11, F_12, F_13,
		F_21, F_22, F_23,
		F_31, F_32, F_33,
		CELL_HAS_ELASTIC_F,
		VOLUMETRIC_STRAIN,
		SHEAR_STRAIN,
		NUM_COMPONENTS
	};

public:

	/// Constructor.
	ElasticFieldWriter(const DelaunayTessellation& tessellation) : FieldWriter(tessellation) {}

	/// Outputs the given components of the elasti field to a VTK file.
	void writeVTKFile(std::ostream& stream, const ElasticField& field, const std::bitset<NUM_COMPONENTS>& components);
};

}; // End of namespace

#endif // __CA_ELASTIC_FIELD_WRITER_H
