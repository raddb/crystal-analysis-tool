///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_ATOMIC_STRUCTURE_WRITER_H
#define __CA_ATOMIC_STRUCTURE_WRITER_H

#include "../../CALib.h"
#include "../../context/CAContext.h"
#include "../../atomic_structure/AtomicStructure.h"
#include "../../pattern/superpattern/SuperPatternAnalysis.h"
#include "../../grains/GrainIdentification.h"

namespace CALib {

/**
 * Writes an AtomicStructure including the analysis results to a file, which can be used for visualization purposes.
 */
class AtomicStructureWriter : public ContextReference
{
public:

	enum DataField {
		ATOM_ID,
		ATOM_TYPE,
		POS_X,
		POS_Y,
		POS_Z,
		SUPER_PATTERN,
		CLUSTER_ID,
		CLUSTER_LOCAL_ID,
		CLUSTER_PROC,
		GRAIN_ID,
		ORIENTATION_001_AXIS,
		F_11, F_12, F_13,
		F_21, F_22, F_23,
		F_31, F_32, F_33,
		SHEAR_STRAIN,
		VOLUMETRIC_STRAIN,
		IS_F_VALID,
		FE_11, FE_12, FE_13,
		FE_21, FE_22, FE_23,
		FE_31, FE_32, FE_33,

		NUM_DATA_FIELDS
	};

public:

	/// Constructor.
	AtomicStructureWriter(CAContext& context) : ContextReference(context) {}

	/// Outputs all atoms to a LAMMPS dump file.
	void writeLAMMPSFile(std::ostream& stream, const std::vector<DataField>& fields,
			const AtomicStructure& structure,
			const SuperPatternAnalysis* patternAnalysis = NULL,
			const GrainIdentification* grainAnalysis = NULL,
			const AtomicStructure* structure2 = NULL,
			CAFloat cutoffRadius = 0);

	/// Outputs all local atoms including ghost atoms to a LAMMPS dump file.
	void writeLocalLAMMPSFile(std::ostream& stream, const AtomicStructure& structure, const SuperPatternAnalysis* patternAnalysis = NULL);

	/// Outputs all atoms to a POSCAR file.
	void writePOSCARFile(std::ostream& stream, const AtomicStructure& structure);

private:

	/// Writes the header of a LAMMPS dump file.
	void writeLAMMPSFileHeader(std::ostream& stream, const AtomicStructure& structure);
};

}; // End of namespace

#endif // __CA_ATOMIC_STRUCTURE_WRITER_H
