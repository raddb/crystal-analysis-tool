///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "HardcodedCoordinationPatternAnalysis.h"
#include "../pattern/catalog/PatternCatalog.h"
#include "../pattern/coordinationpattern/cna/CNACoordinationPattern.h"
#include "../pattern/coordinationpattern/cna/CNACoordinationPatternMatcher.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Searches the input structure for the hardcoded patterns.
******************************************************************************/
void HardcodedCoordinationPatternAnalysis::searchHardcodedCoordinationPatterns()
{
	// Get coordination patterns from catalog.
	const CNACoordinationPattern* fccPattern = NULL;
	const CNACoordinationPattern* hcpPattern = NULL;
	const CNACoordinationPattern* bccPattern = NULL;
	BOOST_FOREACH(const CoordinationPattern& pattern, catalog().coordinationPatterns()) {
		if(pattern.coordinationPatternType != CoordinationPattern::COORDINATION_PATTERN_CNA) continue;
		if(pattern.name == "fcc.1" && pattern.neighbors.size() == 12)
			fccPattern = &static_cast<const CNACoordinationPattern&>(pattern);
		else if(pattern.name == "hcp.1" && pattern.neighbors.size() == 12)
			hcpPattern = &static_cast<const CNACoordinationPattern&>(pattern);
		else if(pattern.name == "bcc.1" && pattern.neighbors.size() == 14)
			bccPattern = &static_cast<const CNACoordinationPattern&>(pattern);
	}
	boost::array<AtomInteger,3> typeCounters;
	typeCounters.assign(0);

	// Reset output array.
	int numAtoms = structure().numLocalAtoms() + structure().numGhostAtoms();
	_coordinationMatchCounts.resize(numAtoms);
	_coordinationMatchesHeads.resize(numAtoms);
	_coordinationPatternMatches.resize(numAtoms);

	// Fill with values.
	std::copy(boost::counting_iterator<int>(0), boost::counting_iterator<int>(_coordinationMatchesHeads.size()),
	         _coordinationMatchesHeads.begin());

	// Assign coordination pattern to atoms.
	vector<int>::iterator matchCount = _coordinationMatchCounts.begin();
	vector<coordmatch>::iterator record = _coordinationPatternMatches.begin();
	for(int atomIndex = 0; atomIndex < numAtoms; atomIndex++, ++matchCount, ++record) {
		*matchCount = 0;

		// Perform common neighbor analysis (CNA) to identify
		// FCC, HCP and BCC atoms.

		int nn = neighborList().neighborCount(atomIndex);
		if(nn != 12 && nn != 14)
			continue;

		// Compute bond bit-flag array.
		NeighborBondArray neighborArray;
		NeighborList::neighbor_iterator n1 = neighborList().neighbors(atomIndex).begin();
		for(int ni1 = 0; ni1 < nn; ni1++, ++n1) {
			neighborArray.setNeighborBond(ni1, ni1, false);
			NeighborList::neighbor_iterator n2 = n1 + 1;
			for(int ni2 = ni1 + 1; ni2 < nn; ni2++, ++n2) {
				bool bonded = (n2->delta - n1->delta).squaredNorm() < neighborList().cutoffSquared();
				neighborArray.setNeighborBond(ni1, ni2, bonded);
			}
		}

		// Compute CNA bond signatures.
		CNABond cnaSignatures[14];
		const CNACoordinationPattern* pattern = NULL;
		if(nn == 12) {
			int n421 = 0, n422 = 0;
			for(int ni = 0; ni < 12; ni++) {
				CNABond& cnaBond = cnaSignatures[ni];
				CNACoordinationPattern::calculateCNASignature(neighborArray, ni, cnaBond, nn);
				if(cnaBond.numCommonNeighbors != 4 || cnaBond.numBonds != 2) break;
				if(cnaBond.maxChainLength == 1) n421++;
				else if(cnaBond.maxChainLength == 2) n422++;
				else break;
			}
			if(n421 == 12) {
				pattern = fccPattern;
				if(structure().isLocalAtom(atomIndex)) typeCounters[0]++;
			}
			else if(n421 == 6 && n422 == 6) {
				pattern = hcpPattern;
				if(structure().isLocalAtom(atomIndex)) typeCounters[1]++;
			}
		}
		else {
			int n444 = 0, n666 = 0;
			for(int ni = 0; ni < 14; ni++) {
				CNABond& cnaBond = cnaSignatures[ni];
				CNACoordinationPattern::calculateCNASignature(neighborArray, ni, cnaBond, nn);
				if(cnaBond.numCommonNeighbors == 4 && cnaBond.numBonds == 4 && cnaBond.maxChainLength == 4) n444++;
				else if(cnaBond.numCommonNeighbors == 6 && cnaBond.numBonds == 6 && cnaBond.maxChainLength == 6) n666++;
				else break;
			}
			if(n444 == 6 && n666 == 8) {
				pattern = bccPattern;
				if(structure().isLocalAtom(atomIndex)) typeCounters[2]++;
			}
		}
		if(!pattern) continue;

		// Initialize permutation maps.
		CoordinationPermutation atomNeighborIndices;
		CoordinationPermutation previousIndices;
		previousIndices.assign(nn, -1);
		atomNeighborIndices.resize(nn);
		for(int n = 0; n < nn; n++)
			atomNeighborIndices[n] = n;

		// Find a matching permutation of the neighbors.
		for(;;) {

			// Validate interconnectivity.
			bool check = false, match = true;
			int validUpTo;
			for(int ni1 = 0; ni1 < nn; ni1++) {
				validUpTo = ni1;
				int atomNeighborIndex1 = atomNeighborIndices[ni1];
				if(atomNeighborIndex1 != previousIndices[ni1]) check = true;
				previousIndices[ni1] = atomNeighborIndex1;
				if(check) {
					if(cnaSignatures[atomNeighborIndex1] != pattern->cnaNeighbors[ni1]) {
						match = false;
						break;
					}
					for(int ni2 = 0; ni2 < ni1; ni2++) {
						int atomNeighborIndex2 = atomNeighborIndices[ni2];
						if(neighborArray.neighborBond(atomNeighborIndex1, atomNeighborIndex2) != pattern->neighborArray.neighborBond(ni1, ni2)) {
							match = false;
							break;
						}
					}
					if(!match) break;
				}
			}
			if(match) {
				// Create a record for this match.
				record->pattern = pattern;
				boost::copy(atomNeighborIndices, record->mapping);
				*matchCount = 1;
				break;
			}

			bitmapSort(atomNeighborIndices.begin() + validUpTo + 1, atomNeighborIndices.end(), nn);
			if(!boost::next_permutation(atomNeighborIndices))
				context().raiseErrorOne("Could not determine neighbor permutation for atom %i.", structure().atomTag(atomIndex));
		}
	}

	// Report number of identified atoms.
	boost::array<AtomInteger,3> totalTypeCounters = parallel().reduce_sum(typeCounters);
	if(parallel().isMaster()) {
		context().msgLogger() << "Found " << totalTypeCounters[0] << " FCC atoms." << endl;
		context().msgLogger() << "Found " << totalTypeCounters[1] << " HCP atoms." << endl;
		context().msgLogger() << "Found " << totalTypeCounters[2] << " BCC atoms." << endl;
		context().msgLogger() << "Found " << (structure().numTotalAtoms() - accumulate(totalTypeCounters, 0)) << " other atoms." << endl;
	}
}

}; // End of namespace
