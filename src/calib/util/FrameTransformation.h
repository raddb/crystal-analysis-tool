///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_FRAME_TRANSFORMATION_H
#define __CA_FRAME_TRANSFORMATION_H

#include "LinAlg.h"

namespace CALib {

/**
 * Helper class that computes the coordinate transformation matrix between two
 * coordinate frames A and B from a set of at least three known linear independent vectors, specified in
 * both coordinate frames.
 */
class FrameTransformation
{
private:

	Matrix3 V, W;

public:

	/// Default constructor.
	FrameTransformation() : V(Matrix3::Zero()), W(Matrix3::Zero()) {}

	/// Adds an input vectors to the matrix calculation for which the representation
	/// in both coordinate frames A and B is known.
	/// The vector v is specified in both coordinate frames A and B.
	void addVector(const Vector3& va, const Vector3& vb) {
		V.noalias() += va * va.transpose();
		W.noalias() += vb * va.transpose();
	}

	/// Computes the matrix that transforms vector coordinates from frame A to frame B exactly.
	/// This assumes that all provided input vectors were consistent, i.e. every vector can be
	/// transformed from A to B by a single transformation matrix.
	///
	/// Generates an error if the resulting matrix is singular, i.e. if the provided vectors are co-planar.
	Matrix3 computeTransformation() const {
		CALIB_ASSERT(!isSingular());
		return (W * V.inverse());
	}

	/// Computes the matrix T that solves the linear system of equations v_B = T * v_A in a least-square sense.
	/// Generates an error if the resulting matrix is singular, i.e. if the provided input vectors are co-planar
	/// (in frame A).
	Matrix3 computeAverageTransformation() const {
		CALIB_ASSERT(!isSingular());
		return (W * V.inverse());
	}

	/// Returns true if the transformation cannot be computed because the V matrix is singular.
	bool isSingular() const {
		return fabs(V.determinant()) <= CAFLOAT_EPSILON;
	}
};

}; // End of namespace

#endif // __CA_FRAME_TRANSFORMATION_H
