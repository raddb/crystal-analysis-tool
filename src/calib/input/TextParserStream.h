///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CRYSTAL_ANALYSIS_TEXT_PARSER_STREAM_H
#define __CRYSTAL_ANALYSIS_TEXT_PARSER_STREAM_H

#include "../CALib.h"

namespace CALib {

/******************************************************************************
* This class is used to read text lines from an input file while
* keeping track of the current line number.
******************************************************************************/
class TextParserStream
{
public:

	/// Constructor.
	TextParserStream(std::istream& stream, const std::string& filename = std::string()) : _file_stream(stream), _lineNumber(0), _filename(filename) {}

	/// Reads in the next line.
	const std::string& readline() {
		using namespace std;

		if(_file_stream.eof())
			throw runtime_error("File parsing error. Unexpected end of file");

		getline(_file_stream, _line);

		if(!_file_stream && !_file_stream.eof())
			throw runtime_error("File parsing error. An I/O error occurred.");

		_lineNumber++;
		return _line;
	}

	void read(void* buffer, std::streamsize n) {
		using namespace std;

		_file_stream.read((char*)buffer, n);
		if(_file_stream.gcount() != n)
			throw runtime_error("File parsing error. Unexpected end of file");
		if(!_file_stream && !_file_stream.eof())
			throw runtime_error("File parsing error. An I/O error occurred.");
	}

	/// Checks whether the end of file is reached.
	bool eof() { return _file_stream.eof(); }

	/// Returns the last line read.
	const std::string& line() const { return _line; }

	/// Returns the line number of the last line read.
	int lineNumber() const { return _lineNumber; }

	/// Returns the name of the file parsed by this parser stream.
	const std::string& filename() const { return _filename; }

protected:

	/// The last text line read from the input stream.
	std::string _line;

	/// The current line number.
	int _lineNumber;

	/// The underlying STL input stream.
	std::istream& _file_stream;

	/// The name of the file parsed by this parser stream.
	std::string _filename;
};

}; // End of namespace

#endif // __CRYSTAL_ANALYSIS_TEXT_PARSER_STREAM_H

