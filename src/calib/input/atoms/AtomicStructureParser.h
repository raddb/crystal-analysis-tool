///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_ATOMIC_STRUCTURE_PARSER_H
#define __CA_ATOMIC_STRUCTURE_PARSER_H

#include "../../CALib.h"
#include "../TextParserStream.h"
#include "../../context/CAContext.h"
#include "../../atomic_structure/AtomicStructure.h"

namespace CALib {

/**
 * This structure stores a set of parameters that control
 * the type of pre-processing performed for atoms after they are read
 * from the input file and before the actual analysis.
 */
struct AtomsPreprocessingParameters
{
	/// Default constructor.
	AtomsPreprocessingParameters() : atomsOffset(Vector3::Zero()), jitterMagnitude(0), cellDeformation(Matrix3::Identity()), cellShape(Matrix3::Zero()), overridePBC(false), boxPadding(0) {
		replicate.assign(1);
	}

	/// Shifts all atoms by a constant vector.
	Vector3 atomsOffset;

	/// Adds a random displacement of the given magnitude to the X,Y,Z coordinates of every atom.
	CAFloat jitterMagnitude;

	/// Applies an affine transformation to all atoms and the simulation cell shape.
	Matrix3 cellDeformation;

	/// Transforms input atoms and cell such that they match the given cell shape.
	/// This preprocessing option is used to eliminate homogeneous deformation.
	Matrix3 cellShape;

	/// If true, the periodic boundary conditions stored in the input file will be overriden by the following flags.
	bool overridePBC;

	/// The user-specified PBC flags.
	boost::array<bool, 3> pbc;

	/// Extra space added to non-periodic dimensions of the simulation box such that all atoms are inside the box.
	/// This is required if atoms are slightly placed outside the simulation cell (happens for LAMMPS dump files).
	CAFloat boxPadding;

	/// The number of times the system is replicated in each spatial direction.
	boost::array<int, 3> replicate;
};

/**
 * Parsers class that reads in atomic structure files.
 */
class AtomicStructureParser : public ContextReference
{
public:

	/// File formats supported by the parser.
	enum FileFormatType {
		FILETYPE_UNKNOWN,				// File format could not be determined.

		FILETYPE_LAMMPS_DUMP,			// LAMMPS text dump files
		FILETYPE_IMD,					// IMD snapshots
		FILETYPE_POSCAR,				// VASP POSCAR files
		FILETYPE_MDPP,					// MD++ snapshots
		FILETYPE_DDCMD,					// ddcMD snapshots
		FILETYPE_COMPACT,				// Proprietary compact binary format
	};

public:

	/// Constructor.
	AtomicStructureParser(AtomicStructure& structure, const AtomsPreprocessingParameters& preprocessing = AtomsPreprocessingParameters()) :
		ContextReference(structure.context()), _structure(structure), _preprocessing(preprocessing), _numInputAtoms(-1), _applyCellTransformation(false) {}

	/// Reads the atomic coordinates from the given input file (or stdin if filename is '-').
	FileFormatType readAtomsFile(const char* filename, std::istream* stdinStream = NULL);

	/// Reads the atomic coordinates from the input stream.
	FileFormatType readAtomsFile(TextParserStream& stream);

	/// Specifies the data columns the parser should look for to read the XYZ coordinates of atoms.
	void setCoordinateColums(const std::string& xcol, const std::string& ycol, const std::string& zcol) {
		_coordinateColumnNameX = xcol;
		_coordinateColumnNameY = ycol;
		_coordinateColumnNameZ = zcol;
	}

protected:

	/// Parses atoms from a LAMMPS dump file.
	void readLAMMPSAtomsFile(TextParserStream& stream);

	/// Parses atoms from an IMD file.
	void readIMDAtomsFile(TextParserStream& stream);

	/// Parses atoms from a ddcMD file.
	void readDDCMDAtomsFile(TextParserStream& stream);

	/// Parses atoms from a VASP POSCAR file.
	void readPOSCARAtomsFile(TextParserStream& stream, CAFloat scaling_factor, const Vector3& firstCellVector);

	/// Parses atoms from a compact binary file.
	void readCompactBinaryAtomsFile(TextParserStream& stream);

protected:

	/// Sets up the simulation cell of the atomic structure.
	virtual void setupSimulationCell(AffineTransformation simulationCell, boost::array<bool,3> pbc);

	/// This structure is used to broadcast atoms read from an input file by the master processor to all other processors.
	struct AtomInfo {
		Vector3 pos;
		int species;
		AtomInteger tag;
	};

	/// Starts the distribution of atoms from the master processor to all processors.
	virtual void beginDistributeAtoms();

	/// Distributes an atom to all processors.
	virtual void distributeAtom(const AtomInfo& atom);

	/// Finishes the distribution of atoms from the master processor to all processors.
	virtual void endDistributeAtoms();

protected:

	/// The structure into which the file is loaded.
	AtomicStructure& _structure;

	/// Controls the processing of atoms.
	AtomsPreprocessingParameters _preprocessing;

	/// The number of atoms being parsed from the input file.
	AtomInteger _numInputAtoms;

	/// Used to transmit atoms to all processors.
	std::vector<AtomInfo> _atomDistributionBuffer;

	/// Points to the next free entry in the buffer.
	std::vector<AtomInfo>::iterator _currentDistributionAtom;

	/// The number of atoms read so far.
	AtomInteger _numAtomsRead;

	/// Indicates that the atomic coordinates are being transformed during parsing.
	bool _applyCellTransformation;

	/// The name of the data column with the X coordinates of atoms.
	std::string _coordinateColumnNameX;

	/// The name of the data column with the Y coordinates of atoms.
	std::string _coordinateColumnNameY;

	/// The name of the data column with the Z coordinates of atoms.
	std::string _coordinateColumnNameZ;
};

}; // End of namespace

#endif // __CA_ATOMIC_STRUCTURE_PARSER_H
