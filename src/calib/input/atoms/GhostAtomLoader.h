///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_GHOST_ATOM_LOADER_H
#define __CA_GHOST_ATOM_LOADER_H

#include "AtomicStructureParser.h"

namespace CALib {

/**
 * Loads the requires ghost atoms from an input file and stores it in an existing AtomicStructure.
 */
class GhostAtomLoader : public AtomicStructureParser
{
public:

	/// Constructor.
	GhostAtomLoader(AtomicStructure& structure, const AtomsPreprocessingParameters& preprocessing = AtomsPreprocessingParameters()) :
		AtomicStructureParser(structure, preprocessing) {}

	/// Loads all required ghost atoms from the decomposed input file.
	void loadGhostAtoms(const std::string& baseDir);

protected:

	/// Sets up the simulation cell of the atomic structure.
	virtual void setupSimulationCell(Matrix3 simulationCell, Point3 simulationCellOrigin, boost::array<bool,3> pbc);

	/// Starts the distribution of atoms from the master processor to all processors.
	virtual void beginDistributeAtoms();

	/// Distributes an atom to all processors.
	virtual void distributeAtom(const AtomInfo& atom);

	/// Finishes the distribution of atoms from the master processor to all processors.
	virtual void endDistributeAtoms();

protected:

	Vector3 shiftVector;
	Point3I procCoord;

	Vector3 cutNormals[3][2];
	CAFloat cutDistances[3][2];
};

}; // End of namespace

#endif // __CA_GHOST_ATOM_LOADER_H
