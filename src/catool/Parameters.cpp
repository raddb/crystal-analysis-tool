///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "CrystalAnalysisTool.h"

/******************************************************************************
* Initializes the program parameters to their default values.
******************************************************************************/
CrystalAnalysisTool::Parameters::Parameters()
{
	maxBurgersCircuitSize = CA_DEFAULT_MAX_BURGERS_CIRCUIT_SIZE;
	maxExtendedBurgersCircuitSize = CA_DEFAULT_MAX_EXTENDED_BURGERS_CIRCUIT_SIZE;
	lineSmoothingLevel = CA_DEFAULT_LINE_SMOOTHING_LEVEL;
	linePointInterval = CA_DEFAULT_LINE_POINT_INTERVAL;
	surfaceSmoothingLevel = CA_DEFAULT_SURFACE_SMOOTHING_LEVEL;
	ghostCutoff = 0;
	allprocOutput = false;
	neighborCutoff = 0;
	helperPointDistance = 0;
	deformationAnalysisMode = false;
	identifyAtomicStructures = false;
	identifyDislocations = false;
	generateTessellation = false;
	generateElasticMapping = false;
	generateInterfaceMesh = false;
	generateDefectSurface = false;
	computeElasticPlasticFields = false;
	computeElasticField = false;
	computeDeformationField = false;
	generateFreeSurface = false;
	identifyGrains = false;
	eliminateHomogeneousDeformation = false;
	grainMisorientationThresholdAngle = -1;
	grainOrientationFluctuationTolerance = 0;
	minimumGrainAtomCount = 0;
	freeSurfaceSphereRadius = 0;
	flipFreeSurfaceOrientation = false;
	crystalPathSteps = 1;
	dontReconstructIdealEdgeVectors = false;
}

/******************************************************************************
* Outputs all parameter descriptions and prints usage information to the console.
******************************************************************************/
void CrystalAnalysisTool::Parameters::printHelp(ostream& stream)
{
	stream << "Crystal Analysis Tool" << endl;
	stream << "Author: Alexander Stukowski (stukowski@mm.tu-darmstadt.de)" << endl;
	stream << "Version: " << CA_LIB_VERSION_STRING << endl << endl;
	stream << "Usage: CrystalAnalysis [options] <inputfile> [deformedconf]" << endl << endl;
	stream << "Parameters:" << endl << endl;
	stream << "    inputfile          : Input atoms file (LAMMPS, ddcMD, POSCAR or IMD format)" << endl;
	stream << "    deformedconf       : Optional deformed simulation snapshot for deformation analysis mode" << endl;
	stream << endl << "Output options:" << endl << endl;
	stream << "    -cell FILE.vtk                : Output simulation cell geometry to a VTK file" << endl;
	stream << "    -atoms FIELDS FILE.dump.gz    : Output processed atoms to a LAMMPS dump file" << endl;
	stream << "    -output FILE.ca               : Output dislocation data to file, which can be opened with OVITO" << endl;
	stream << "    -scounts FILE.txt             : Output number of matching atoms for each structure pattern to a text file" << endl;
	stream << "    -clusters FILE.vtk            : Output clusters to a VTK file" << endl;
	stream << "    -mesh FILE.vtk                : Output interface mesh to a VTK file (for debugging only)" << endl;
	stream << "    -dislocations FILE.vtk        : Output dislocations to a VTK file" << endl;
	stream << "    -junctions FILE.txt           : Output list of dislocation junctions to a text file" << endl;
	stream << "    -defectsurface FILE.vtk       : Output crystal defect surface to a VTK file" << endl;
	stream << "    -defectsurfacecap FILE.vtk    : Output PBC cap of defect surface mesh to a VTK file" << endl;
	stream << "    -freesurface RADIUS FILE.vtk  : Compute and output free surfaces to a VTK file" << endl;
	stream << "    -freesurfacecap FILE.vtk      : Output PBC cap of free surface mesh to a VTK file" << endl;
	stream << "    -elasticfield COMPTS FILE.vtk : Output the given components of the elastic field to a VTK file" << endl;
	stream << "    -deformfield COMPTS FILE.vtk  : Output the given components of the deformation field to a VTK file" << endl;
	//stream << "    -grains A B C FILE.vtk        : Output list of identified grains to a text file" << endl;
	stream << "    -logall                       : Show log output of all MPI processors" << endl;
	stream << endl << "Input and preprocessing options:" << endl << endl;
	stream << "    -pbc X Y Z            :  Specify periodic boundary conditions (X,Y,Z = 0 or 1)" << endl;
	stream << "    -offset X Y Z         :  Add an offset to all atomic positions prior to analysis" << endl;
	stream << "    -scale X Y Z          :  Scales the simulation cell prior to analysis" << endl;
	stream << "    -multiply X Y Z       :  Replicates the periodic simulation cell prior to analysis" << endl;
	stream << "    -padding SIZE         :  Extra padding of the simulation cell added in non-periodic directions" << endl;
	stream << "    -xyzcol XCOL YCOL ZCOL:  Specifies the names of the x,y,z data columns in the input file." << endl;
	stream << "    -nohomogeneous        :  Eliminate homogeneous deformation before performing a deformation analysis" << endl;
	stream << "    -jitter STRENGTH      :  Randomly displace atomic positions prior to analysis (for debugging only)" << endl;
	stream << endl << "Control options:" << endl << endl;
	stream << "    -catalog FILE         :  Name of the atomic pattern catalog file" << endl;
	stream << "    -patterns LIST        :  Comma-separated list of structures from the pattern catalog to search for" << endl;
	stream << "    -cutoff RADIUS        :  Build conventional neighbor lists with given cutoff radius" << endl;
	stream << "    -ghost SIZE           :  Controls overlap size between processor domains" << endl;
	stream << "    -maxcircuitsize N     :  Maximum Burgers circuit length during first tracing phase (default N=" << CA_DEFAULT_MAX_BURGERS_CIRCUIT_SIZE << ")" << endl;
	stream << "    -extcircuitsize N     :  Maximum Burgers circuit length during second tracing phase (default N=" << CA_DEFAULT_MAX_EXTENDED_BURGERS_CIRCUIT_SIZE << ")" << endl;
	stream << "    -pointinterval X      :  Sampling point distance for dislocation lines (default X=" << CA_DEFAULT_LINE_POINT_INTERVAL << ")" << endl;
	stream << "    -smoothlines N        :  Smoothing level for dislocation lines (default N=" << CA_DEFAULT_LINE_SMOOTHING_LEVEL << ")" << endl;
	stream << "    -smoothsurface N      :  Smoothing level for defect surface mesh (default N=" << CA_DEFAULT_SURFACE_SMOOTHING_LEVEL << ")" << endl;
	stream << "    -crystalpathsteps N   :  Max distance when determining atom-to-atom steps (default N=" << crystalPathSteps << ")" << endl;
	stream << "    -dontreconstruct      :  Do not try to reconstruct ideal edge vectors" << endl;
	stream << endl;
}

/******************************************************************************
* Checks if all command line parameters specified by the user are valid.
******************************************************************************/
void CrystalAnalysisTool::validateParameters()
{
	// Consistency check of command line options.

	if(params().neighborCutoff <= 0.0)
		context().raiseErrorAll("Invalid neighbor cutoff. Please use the -cutoff option to specify a neighbor cutoff distance.");

	params().helperPointDistance = params().neighborCutoff * CA_DEFAULT_HELPER_POINT_DISTANCE;

	// The user can override this with the -ghost switch.
	if(params().ghostCutoff == 0)
		params().ghostCutoff = params().neighborCutoff * CA_DEFAULT_GHOST_ATOM_LAYER_THICKNESS;
	if(params().ghostCutoff <= 0.0)
		context().raiseErrorAll("Invalid ghost cutoff. Please use the -ghost option to specify a valid overlap distance.");
	if(params().ghostCutoff < params().neighborCutoff * 2.0)
		context().raiseErrorAll("Invalid ghost cutoff. Ghost cutoff must not be be smaller than twice the neighbor cutoff.");

	if(params().deformationFieldOutputFile.empty() == false && !params().deformationAnalysisMode)
		context().raiseErrorAll("A deformed configuration file must be specified when computing the deformation field with the -deformfield option.");

	if(params().maxBurgersCircuitSize < 3)
		context().raiseErrorAll("Invalid settings: Maximum Burgers circuit length (-maxcircuitsize) must be at least 3.");
	if(params().maxBurgersCircuitSize > params().maxExtendedBurgersCircuitSize)
		context().raiseErrorAll("Invalid settings: Maximum Burgers circuit length (-maxcircuitsize) must not be larger than the extended Burgers circuit limit (-extcircuitsize).");

	if(params().identifyGrains && parallel().processorCount() != 1)
		context().raiseErrorAll("Cannot use the '-grains' command option in parallel mode. The grain identification is not parallelized.");
	if(params().identifyGrains && params().grainMisorientationThresholdAngle <= 0.0)
		context().raiseErrorAll("Invalid settings in '-grains' option: Grain lattice misorientation threshold must be positive.");
	if(params().identifyGrains && params().grainOrientationFluctuationTolerance < 0.0)
		context().raiseErrorAll("Invalid settings in '-grains' option: Grain orientation fluctuation tolerance must be positive.");

	// Parse per-atom data fields to be output.
	istringstream ss0(params().atomsOutputFieldsString);
	string fieldName;
	while(getline(ss0, fieldName, ',')) {
		if(fieldName == "id")
			params().atomsOutputFields.push_back(AtomicStructureWriter::ATOM_ID);
		else if(fieldName == "type")
			params().atomsOutputFields.push_back(AtomicStructureWriter::ATOM_TYPE);
		else if(fieldName == "x")
			params().atomsOutputFields.push_back(AtomicStructureWriter::POS_X);
		else if(fieldName == "y")
			params().atomsOutputFields.push_back(AtomicStructureWriter::POS_Y);
		else if(fieldName == "z")
			params().atomsOutputFields.push_back(AtomicStructureWriter::POS_Z);
		else if(fieldName == "pattern") {
			params().atomsOutputFields.push_back(AtomicStructureWriter::SUPER_PATTERN);
			params().identifyAtomicStructures = true;
		}
		else if(fieldName == "cluster") {
			params().atomsOutputFields.push_back(AtomicStructureWriter::CLUSTER_ID);
			params().identifyAtomicStructures = true;
		}
		else if(fieldName == "cluster_local_id") {
			params().atomsOutputFields.push_back(AtomicStructureWriter::CLUSTER_LOCAL_ID);
			params().identifyAtomicStructures = true;
		}
		else if(fieldName == "cluster_proc") {
			params().atomsOutputFields.push_back(AtomicStructureWriter::CLUSTER_PROC);
			params().identifyAtomicStructures = true;
		}
		else if(fieldName == "grain") {
			params().atomsOutputFields.push_back(AtomicStructureWriter::GRAIN_ID);
			if(!params().identifyGrains)
				context().raiseErrorAll("Invalid '-atoms' command option: You also have to use the '-grains' command line option when using the 'grain' output field.");
		}
		else if(fieldName == "orient_001") {
			params().atomsOutputFields.push_back(AtomicStructureWriter::ORIENTATION_001_AXIS);
			params().identifyAtomicStructures = true;
		}
		else if(fieldName == "shear_strain") {
			params().atomsOutputFields.push_back(AtomicStructureWriter::SHEAR_STRAIN);
			if(!params().deformationAnalysisMode)
				context().raiseErrorAll("A deformed configuration file must be specified when computing atomic shear strain.");
		}
		else if(fieldName == "volumetric_strain") {
			params().atomsOutputFields.push_back(AtomicStructureWriter::VOLUMETRIC_STRAIN);
			if(!params().deformationAnalysisMode)
				context().raiseErrorAll("A deformed configuration file must be specified when computing the atomic strain.");
		}
		else if(fieldName == "F_valid") {
			params().atomsOutputFields.push_back(AtomicStructureWriter::IS_F_VALID);
		}
		else {
			bool invalidName = true;
			for(int i = 0; i < 3; i++) {
				for(int j = 0; j < 3; j++) {
					ostringstream name1;
					name1 << "F" << (i+1) << (j+1);
					if(fieldName == name1.str()) {
						params().atomsOutputFields.push_back(AtomicStructureWriter::DataField(AtomicStructureWriter::F_11 + i*3 + j));
						if(!params().deformationAnalysisMode)
							context().raiseErrorAll("A deformed configuration file must be specified when computing atomic deformation tensors.");
						invalidName = false;
						break;
					}
					ostringstream name2;
					name2 << "FE" << (i+1) << (j+1);
					if(fieldName == name2.str()) {
						params().atomsOutputFields.push_back(AtomicStructureWriter::DataField(AtomicStructureWriter::FE_11 + i*3 + j));
						params().identifyAtomicStructures = true;
						invalidName = false;
						break;
					}
				}
			}
			if(invalidName)
				context().raiseErrorAll("Invalid per-atom field after -atoms option: %s", fieldName.c_str());
		}
	}

	// Parse the field components to be output for the deformation field.
	istringstream ss1(params().deformationFieldOutputComponentsString);
	string componentName;
	while(getline(ss1, componentName, ',')) {
		if(componentName == "vol")
			params().deformationFieldOutputComponents.set(DeformationFieldWriter::CELL_VOLUME);
		else if(componentName == "aspect")
			params().deformationFieldOutputComponents.set(DeformationFieldWriter::CELL_ASPECT_RATIO);
		else if(componentName == "hasFP")
			params().deformationFieldOutputComponents.set(DeformationFieldWriter::CELL_HAS_PLASTIC_F);
		else if(componentName == "plastic_shear")
			params().deformationFieldOutputComponents.set(DeformationFieldWriter::PLASTIC_SHEAR_STRAIN);
		else {
			bool invalidName = true;
			for(int i = 0; i < 3; i++) {
				for(int j = 0; j < 3; j++) {
					ostringstream fieldName;
					fieldName << "F" << (i+1) << (j+1);
					if(componentName == fieldName.str()) {
						params().deformationFieldOutputComponents.set(DeformationFieldWriter::F_11 + i*3 + j);
						invalidName = false;
						break;
					}
				}
			}
			for(int i = 0; i < 3; i++) {
				for(int j = 0; j < 3; j++) {
					ostringstream fieldName;
					fieldName << "FP" << (i+1) << (j+1);
					if(componentName == fieldName.str()) {
						params().deformationFieldOutputComponents.set(DeformationFieldWriter::F_plastic_11 + i*3 + j);
						invalidName = false;
						break;
					}
				}
			}
			if(invalidName)
				context().raiseErrorAll("Invalid deformation field component after -deformfield option: %s", componentName.c_str());
		}
	}

	// Parse the field components to be output for the elastic field.
	istringstream ss2(params().elasticFieldOutputComponentsString);
	while(getline(ss2, componentName, ',')) {
		if(componentName == "vol")
			params().elasticFieldOutputComponents.set(ElasticFieldWriter::CELL_VOLUME);
		else if(componentName == "aspect")
			params().elasticFieldOutputComponents.set(ElasticFieldWriter::CELL_ASPECT_RATIO);
		else if(componentName == "valid")
			params().elasticFieldOutputComponents.set(ElasticFieldWriter::CELL_HAS_ELASTIC_F);
		else if(componentName == "shear")
			params().elasticFieldOutputComponents.set(ElasticFieldWriter::SHEAR_STRAIN);
		else if(componentName == "volumetric")
			params().elasticFieldOutputComponents.set(ElasticFieldWriter::VOLUMETRIC_STRAIN);
		else {
			bool invalidName = true;
			for(int i = 0; i < 3; i++) {
				for(int j = 0; j < 3; j++) {
					ostringstream fieldName;
					fieldName << "F" << (i+1) << (j+1);
					if(componentName == fieldName.str()) {
						params().elasticFieldOutputComponents.set(ElasticFieldWriter::F_11 + i*3 + j);
						invalidName = false;
						break;
					}
				}
			}
			if(invalidName)
				context().raiseErrorAll("Invalid elastic field component after -elasticfield option: %s", componentName.c_str());
		}
	}

	params().computeElasticField = !params().elasticFieldOutputFile.empty();
	params().computeElasticPlasticFields = !params().deformationFieldOutputComponentsString.empty();
	params().generateDefectSurface = !params().surfaceOutputFile.empty() || !params().surfaceCapOutputFile.empty() || !params().crystalAnalysisOutputFile.empty();
	params().identifyDislocations = !params().dislocationsOutputFile.empty() || !params().crystalAnalysisOutputFile.empty() || !params().junctionsOutputFile.empty();
	params().generateInterfaceMesh = params().identifyDislocations || params().generateDefectSurface || !params().meshOutputFile.empty();
	params().generateElasticMapping = params().generateInterfaceMesh || params().computeElasticPlasticFields || params().computeElasticField || !params().elasticFieldOutputFile.empty();
	params().generateTessellation = params().generateElasticMapping || params().computeDeformationField;
	params().generateFreeSurface = !params().freeSurfaceOutputFile.empty() || !params().freeSurfaceCapOutputFile.empty() || !params().freeSurfaceRawOutputFile.empty();
	params().identifyAtomicStructures |= params().generateElasticMapping || params().computeElasticPlasticFields || params().computeElasticField || params().identifyGrains || !params().structureCountsOutputFile.empty();

	if(params().computeDeformationField && !params().deformationAnalysisMode)
		context().raiseErrorAll("A deformed configuration file must be specified when computing the deformation field.");

	if(params().identifyAtomicStructures && params().patternCatalogFile.empty())
		context().raiseErrorAll("When identifying atomic structures the -catalog option must be used to specify the pattern catalog file.");

	if(params().identifyAtomicStructures && params().patternList.empty())
		context().raiseErrorAll("When identifying atomic structures the -patterns option must be used to select some search patterns.");

	if(params().generateFreeSurface && params().freeSurfaceSphereRadius <= 0)
		context().raiseErrorAll("Sphere radius for free surface identification must be positive.");

	if(params().generateFreeSurface && params().helperPointDistance < params().freeSurfaceSphereRadius * 2.0)
		context().raiseErrorAll("Tessellation helper point distance is too small for the given free-surface identification sphere radius. "
				"Please increase the value of the -cutoff parameter to %f.", params().freeSurfaceSphereRadius * 2.0 / CA_DEFAULT_HELPER_POINT_DISTANCE);

	_tracer.setMaximumBurgersCircuitSize(params().maxBurgersCircuitSize);
	_tracer.setMaximumExtendedBurgersCircuitSize(params().maxExtendedBurgersCircuitSize);
}

/******************************************************************************
* Parses command line parameters.
******************************************************************************/
bool CrystalAnalysisTool::Parameters::parseCommandLine(int argc, char* argv[], ostream& err)
{
	// Parse command line parameters.
	int iarg = 1;
	while(iarg < argc) {
		if(strcmp(argv[iarg], "-atoms") == 0) {
			if(iarg >= argc-2) {
				err << "Missing parameter(s) after " << argv[iarg] << endl;
				return false;
			}
			atomsOutputFieldsString = argv[iarg+1];
			atomsOutputFile = argv[iarg+2];
			iarg += 3;
		}
		else if(strcmp(argv[iarg], "-cell") == 0) {
			if(iarg >= argc-1) {
				err << "Missing parameter after " << argv[iarg] << endl;
				return false;
			}
			cellOutputFile = argv[iarg+1];
			iarg += 2;
		}
		else if(strcmp(argv[iarg], "-scounts") == 0) {
			if(iarg >= argc-1) {
				err << "Missing parameter after " << argv[iarg] << endl;
				return false;
			}
			structureCountsOutputFile = argv[iarg+1];
			iarg += 2;
		}
		else if(strcmp(argv[iarg], "-clusters") == 0) {
			if(iarg >= argc-1) {
				err << "Missing parameter after " << argv[iarg] << endl;
				return false;
			}
			clustersOutputFile = argv[iarg+1];
			iarg += 2;
		}
		else if(strcmp(argv[iarg], "-mesh") == 0) {
			if(iarg >= argc-1) {
				err << "Missing parameter after " << argv[iarg] << endl;
				return false;
			}
			meshOutputFile = argv[iarg+1];
			iarg += 2;
		}
		else if(strcmp(argv[iarg], "-output") == 0) {
			if(iarg >= argc-1) {
				err << "Missing parameter after " << argv[iarg] << endl;
				return false;
			}
			crystalAnalysisOutputFile = argv[iarg+1];
			iarg += 2;
		}
		else if(strcmp(argv[iarg], "-dislocations") == 0) {
			if(iarg >= argc-1) {
				err << "Missing parameter after " << argv[iarg] << endl;
				return false;
			}
			dislocationsOutputFile = argv[iarg+1];
			iarg += 2;
		}
		else if(strcmp(argv[iarg], "-junctions") == 0) {
			if(iarg >= argc-1) {
				err << "Missing parameter after " << argv[iarg] << endl;
				return false;
			}
			junctionsOutputFile = argv[iarg+1];
			iarg += 2;
		}
		else if(strcmp(argv[iarg], "-defectsurface") == 0) {
			if(iarg >= argc-1) {
				err << "Missing parameter after " << argv[iarg] << endl;
				return false;
			}
			surfaceOutputFile = argv[iarg+1];
			iarg += 2;
		}
		else if(strcmp(argv[iarg], "-defectsurfacecap") == 0) {
			if(iarg >= argc-1) {
				err << "Missing parameter after " << argv[iarg] << endl;
				return false;
			}
			surfaceCapOutputFile = argv[iarg+1];
			iarg += 2;
		}
		else if(strcmp(argv[iarg], "-deformfield") == 0) {
			if(iarg >= argc-2) {
				err << "Missing parameter after " << argv[iarg] << endl;
				return false;
			}
			deformationFieldOutputComponentsString = argv[iarg+1];
			deformationFieldOutputFile = argv[iarg+2];
			computeDeformationField = true;
			iarg += 3;
		}
		else if(strcmp(argv[iarg], "-elasticfield") == 0) {
			if(iarg >= argc-2) {
				err << "Missing parameter after " << argv[iarg] << endl;
				return false;
			}
			elasticFieldOutputComponentsString = argv[iarg+1];
			elasticFieldOutputFile = argv[iarg+2];
			iarg += 3;
		}
		else if(strcmp(argv[iarg], "-maxcircuitsize") == 0) {
			if(iarg >= argc-1) {
				err << "Missing parameter after " << argv[iarg] << endl;
				return false;
			}
			maxBurgersCircuitSize = atoi(argv[iarg+1]);
			iarg += 2;
		}
		else if(strcmp(argv[iarg], "-extcircuitsize") == 0) {
			if(iarg >= argc-1) {
				err << "Missing parameter after " << argv[iarg] << endl;
				return false;
			}
			maxExtendedBurgersCircuitSize = atoi(argv[iarg+1]);
			iarg += 2;
		}
		else if(strcmp(argv[iarg], "-crystalpathsteps") == 0) {
			if(iarg >= argc-1) {
				err << "Missing parameter after " << argv[iarg] << endl;
				return false;
			}
			crystalPathSteps = atoi(argv[iarg+1]);
			iarg += 2;
		}
		else if(strcmp(argv[iarg], "-jitter") == 0) {
			if(iarg >= argc-1) {
				err << "Missing parameter after " << argv[iarg] << endl;
				return false;
			}
			preprocessing.jitterMagnitude = atof(argv[iarg+1]);
			iarg += 2;
		}
		else if(strcmp(argv[iarg], "-cutoff") == 0) {
			if(iarg >= argc-1) {
				err << "Missing parameter after " << argv[iarg] << endl;
				return false;
			}
			neighborCutoff = atof(argv[iarg+1]);
			iarg += 2;
		}
		else if(strcmp(argv[iarg], "-ghost") == 0) {
			if(iarg >= argc-1) {
				err << "Missing parameter after " << argv[iarg] << endl;
				return false;
			}
			ghostCutoff = atof(argv[iarg+1]);
			iarg += 2;
		}
		else if(strcmp(argv[iarg], "-padding") == 0) {
			if(iarg >= argc-1) {
				err << "Missing parameter after " << argv[iarg] << endl;
				return false;
			}
			preprocessing.boxPadding = atof(argv[iarg+1]);
			iarg += 2;
		}
		else if(strcmp(argv[iarg], "-pbc") == 0) {
			if(iarg >= argc-3) {
				err << "Missing parameter after " << argv[iarg] << endl;
				return false;
			}
			preprocessing.pbc[0] = atoi(argv[iarg+1]);
			preprocessing.pbc[1] = atoi(argv[iarg+2]);
			preprocessing.pbc[2] = atoi(argv[iarg+3]);
			preprocessing.overridePBC = true;
			iarg += 4;
		}
		else if(strcmp(argv[iarg], "-multiply") == 0) {
			if(iarg >= argc-3) {
				err << "Missing parameter after " << argv[iarg] << endl;
				return false;
			}
			preprocessing.replicate[0] = atoi(argv[iarg+1]);
			preprocessing.replicate[1] = atoi(argv[iarg+2]);
			preprocessing.replicate[2] = atoi(argv[iarg+3]);
			iarg += 4;
		}
		else if(strcmp(argv[iarg], "-offset") == 0) {
			if(iarg >= argc-3) {
				err << "Missing parameter after " << argv[iarg] << endl;
				return false;
			}
			preprocessing.atomsOffset.x() = atof(argv[iarg+1]);
			preprocessing.atomsOffset.y() = atof(argv[iarg+2]);
			preprocessing.atomsOffset.z() = atof(argv[iarg+3]);
			iarg += 4;
		}
		else if(strcmp(argv[iarg], "-scale") == 0) {
			if(iarg >= argc-3) {
				err << "Missing parameter after " << argv[iarg] << endl;
				return false;
			}
			Vector3 scaleFactors;
			scaleFactors.x() = atof(argv[iarg+1]);
			scaleFactors.y() = atof(argv[iarg+2]);
			scaleFactors.z() = atof(argv[iarg+3]);
			preprocessing.cellDeformation = Eigen::Scaling(scaleFactors) * preprocessing.cellDeformation;
			iarg += 4;
		}
		else if(strcmp(argv[iarg], "-smoothlines") == 0) {
			if(iarg >= argc-1) {
				err << "Missing parameter after " << argv[iarg] << endl;
				return false;
			}
			lineSmoothingLevel = atoi(argv[iarg+1]);
			iarg += 2;
		}
		else if(strcmp(argv[iarg], "-pointinterval") == 0) {
			if(iarg >= argc-1) {
				err << "Missing parameter after " << argv[iarg] << endl;
				return false;
			}
			linePointInterval = atof(argv[iarg+1]);
			iarg += 2;
		}
		else if(strcmp(argv[iarg], "-smoothsurface") == 0) {
			if(iarg >= argc-1) {
				err << "Missing parameter after " << argv[iarg] << endl;
				return false;
			}
			surfaceSmoothingLevel = atoi(argv[iarg+1]);
			iarg += 2;
		}
		else if(strcmp(argv[iarg], "-grains") == 0) {
			if(iarg >= argc-4) {
				err << "Missing parameter after " << argv[iarg] << endl;
				return false;
			}
			grainMisorientationThresholdAngle = atof(argv[iarg+1]);
			grainOrientationFluctuationTolerance = atof(argv[iarg+2]);
			minimumGrainAtomCount = atoi(argv[iarg+3]);
			grainsOutputFile = argv[iarg+4];
			identifyGrains = true;
			iarg += 5;
		}
		else if(strcmp(argv[iarg], "-freesurface") == 0) {
			if(iarg >= argc-2) {
				err << "Missing parameter after " << argv[iarg] << endl;
				return false;
			}
			freeSurfaceSphereRadius = atof(argv[iarg+1]);
			freeSurfaceOutputFile = argv[iarg+2];
			iarg += 3;
		}
		else if(strcmp(argv[iarg], "-freesurfacecap") == 0) {
			if(iarg >= argc-1) {
				err << "Missing parameter after " << argv[iarg] << endl;
				return false;
			}
			freeSurfaceCapOutputFile = argv[iarg+1];
			iarg += 2;
		}
		else if(strcmp(argv[iarg], "-freesurfaceraw") == 0) {
			if(iarg >= argc-2) {
				err << "Missing parameter after " << argv[iarg] << endl;
				return false;
			}
			freeSurfaceSphereRadius = atof(argv[iarg+1]);
			freeSurfaceRawOutputFile = argv[iarg+2];
			iarg += 3;
		}
		else if(strcmp(argv[iarg], "-logall") == 0) {
			allprocOutput = true;
			iarg += 1;
		}
		else if(strcmp(argv[iarg], "-nohomogeneous") == 0) {
			eliminateHomogeneousDeformation = true;
			iarg += 1;
		}
		else if(strcmp(argv[iarg], "-xyzcol") == 0) {
			if(iarg >= argc-3) {
				err << "Missing parameter(s) after " << argv[iarg] << endl;
				return false;
			}
			inputCoordinateColumnX = argv[iarg+1];
			inputCoordinateColumnY = argv[iarg+2];
			inputCoordinateColumnZ = argv[iarg+3];
			iarg += 4;
		}
		else if(strcmp(argv[iarg], "-catalog") == 0) {
			if(iarg >= argc-1) {
				err << "Missing parameter(s) after " << argv[iarg] << endl;
				return false;
			}
			patternCatalogFile = argv[iarg+1];
			iarg += 2;
		}
		else if(strcmp(argv[iarg], "-patterns") == 0) {
			if(iarg >= argc-1) {
				err << "Missing parameter(s) after " << argv[iarg] << endl;
				return false;
			}
			patternList = argv[iarg+1];
			iarg += 2;
		}
		else if(strcmp(argv[iarg], "-dontreconstruct") == 0) {
			dontReconstructIdealEdgeVectors = true;
			iarg += 1;
		}
		else if(argv[iarg][0] == '-') {
			err << "Invalid command line option: " << argv[iarg] << endl;
			return false;
		}
		else break;
	}
	if(iarg + 1 != argc && iarg + 2 != argc) {
		err << "Missing or invalid command line parameters." << endl;
		printHelp(err);
		return false;
	}

	inputFile = argv[iarg++];
	if(iarg < argc) {
		deformedConfigurationFile = argv[iarg++];
		deformationAnalysisMode = true;
	}

	return true;
}
